#!/bin/bash
#set -x
[ -n "$TERM" ] && clear
########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: GNU/LGPL
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#	This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2017/04/22
#
##
###
########################################################################
###
##
#   For GNU/Linux: Bash (tested on Debian Stable|Sid)
##
###
########################################################################

export LC_ALL=C


IPv6=0
RACINE="$(dirname "$(readlink -f -- "$0")")"
DIR_DL="${RACINE}/downloads"
DIR_LISTS="${RACINE}/lists"
DIR_SRC="${RACINE}/src"

list="bogons"

now="$(date +"%x %X")"
today="$(date +%s)"

seconds=86400   # in seconds

declare -a blocklists

###
#
# Functions
#
###

### Get data into file in var list...
build_blocklists() {

    printf "%s \n" "*** Manage list: ${DIR_SRC}/${list} ***"

    if [ -f "${DIR_SRC}/${list}" ]; then

        i=0
        while read -r line; do
            #if [ "$(echo "${line}" | grep -v "^#")" ]; then blocklists[$i]="${line}"; fi
            if echo "${line}" | grep -v "^#"; then blocklists[$i]="${line}"; fi
            let i++
        done < "${DIR_SRC}/${list}"
        unset i

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "Le fichier ${DIR_SRC}/${list} semble ne pas être lisible !"
        exit 1

    fi

    }
    
build_sums() {

    if [ -f "${DIR_LISTS}/${file}" ];  then
    
		printf "%s \n" "===> Attempt to build checksum file: ${DIR_LISTS}/${file}"

        cd "${DIR_LISTS}" || exit 1
        
        if sha512sum --tag "${file}" > "${file}.sha512"; then
        
			printf "[ \\33[0;32m%s\\33[0;39m ] %s \n" "OK" "The checksum file is correctly created!"
        
        else
		
			printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem to create checksum file!"
        
        fi
        
        cd "${RACINE}" || exit 1

    fi

    }
    
del_files() {
	
	printf "%s \n" "########## Deleting old files ##########"
	
	for f in "${DIR_LISTS}/fullbogons-ipv4.txt" "${DIR_LISTS}/fullbogons-ipv4.txt.sha512"; do
	
		echo "file: $f"
		
		if [ -f "${f}" ]; then
		
			if rm "${f}"; then
				printf "[ \\33[0;32m%s\\33[0;39m ] %s \n" "OK" "The file ${f} is correctly deleted!"
			else
				printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem to delete file: ${f}!"
			fi
		
		fi
		
	done
	
}

download() {

    printf "%s \n" "########## Attempt to get blocklists files ##########"
    printf "%s \n" "=> Attempt to download file: ${filename}"

    local bool=0
    
    [ ! -d "${DIR_DL}" ] && mkdir "${DIR_DL}"

    if [ -x "$(which curl)" ]; then

        if ! "$(which curl)" -A "Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -o "${filename}" "${url}"; then
            bool=1
        fi

    elif [ -x "$(which wget)" ]; then

        if ! "$(which wget)" --user-agent="Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -c -O "${filename}" "${url}"; then
            bool=1
        fi

	fi

    if [ ${bool} -eq 0 ]; then

        printf "[ \\33[0;32m%s\\33[0;39m ] %s \n" "OK" "The file ${filename} is correctly downloaded!"

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem with download file ${filename}!"
        exit 1

    fi

    }
    
make_uniq_list() {
	
	printf "%s \n" "====> Attempt to make uniq file with filename: ${filename}"
	
	case "${file}" in
	
		"fullbogons-ipv4.txt")
		
			printf "%s \n" "=> Check ipv4!"
			valid_ipv4 "${filename}" >> "${DIR_LISTS}/${file}"
		
		;;
        
		"fullbogons-ipv6.txt")
    
			printf "%s \n" "=> Check ipv6!"
			valid_ipv6 "${filename}" >> "${DIR_LISTS}/${file}"
	
		;;
		
	esac
	
	#mime="text/plain"
    
    #if [ "$(file -b -i "${filename}")" = "${mime}" ]; then

        #printf "%s \n" "====> Attempt to make uniq file with filename: ${filename}"

        #while read -r line; do
            
            #if [ "${file}" == "fullbogons-ipv6.txt" ]; then
			
				##if contains "${line}" ":" ; then 
				#if valid_ipv6 "${line}"; then
					##echo "${line}" >> "${DIR_SRC}/${list}_ipv6"
					#echo "${line}" >> "${DIR_LISTS}/${file}"
				#fi
			
			#else
			
				##if contains "${line}" "." ; then 
				#if valid_ipv4 "${line}"; then
					##echo "${line}" >> "${DIR_SRC}/${list}_ipv4"
					#echo "${line}" >> "${DIR_LISTS}/${file}"
				#fi
			
			#fi
            
        #done < "${filename}"

    ##fi

    #unset mime
	
}

# Create uniq list file by datas into array blocklist
mng_blocklists() {

    printf "%s \n" "### Essai de lecture des donnees blocklist"

    count="${#blocklists[@]}"

    if [[ "${count}" -gt 0 ]]; then

        for url in "${blocklists[@]}"; do

			#ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			#name="${ndd}_${file}"
			filename="${DIR_DL}/${file}"
			
            printf "file: %s \n" "${file}"
            printf "filename: %s \n" "${filename}"

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${today} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then

                    download

                fi

            else

                download

            fi
            
            if [ -f "${filename}" ]; then
				
				purge_files
				
				#cp "${filename}" "${DIR_LISTS}/${file}"
				make_uniq_list
				
				build_sums
				
            fi 

            unset filename

        done

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "Il semble ne pas y avoir de données récupérées !"
        exit 1

    fi

    unset count

    }

purge_files() {

    printf "%s \n" "===> Attempt to transform downloaded file: ${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one
    
    sed -i -e "/^$/d;/^#/d;/^[[:space:]]*$/d;" "${filename}"
    
    case "${file}" in
		"fullbogons-ipv4.txt")
			sed -i -e "s/192\.168\(.*\)/#192.168\1/" "${filename}"
		;;
		#"fullbogons-ipv6.txt")
			#sed -i -e "" "${filename}"
		#;;
    esac

    }

# functions valid_ip**() inspired 
# by: https://helloacm.com/how-to-valid-ipv6-addresses-using-bash-and-regex/
valid_ipv4() {
	
	egrep '^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/?([0-9]{1,2})?)$' "${1}"
	
	}
	
valid_ipv6() {

	egrep '^([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])$' "${1}"
	
	}
#valid_ipv4() {
	
	#if [[ "$1" =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]; then
		#return 0;
	#else
		#return 1;
	#fi
	
#}

#valid_ipv6() {
	
	#if [[ "$1" =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$ ]]; then
		#return 0;
	#else
		#return 1;
	#fi
	
#}

del_files

build_blocklists

mng_blocklists
