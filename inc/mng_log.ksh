########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

setLogs() {

    if [ "${debug}" -eq 1 ]; then

        if [ ! -d "${DIR_LOG}" ]; then mkdir "${DIR_LOG}"; fi

        mode="normal"
        [ "${cron}" -eq 1 ] && mode="cron"
        [ "${dialog}" -eq 1 ] && mode="dialog"
        [ "${verbose}" -eq 1 ] && mode="verbose"

        case "${use_timestamp}" in
            0) bz_log="${DIR_LOG}/${today}.log" ;;
            1) bz_log="${DIR_LOG}/${timestamp}.log"
        esac

        _mssg "${bz_log}" "#"

        if touch "${bz_log}"; then
            _log "##############################
### BlockZones :: File log to debug if need!
### Mode: ${mode}
### Date: ${today}
### Script: $0
### Tool to download: ${dldr}
##############################
## color: ${use_color}
## on checksum file: ${one_checksum_file}
## use signify: ${use_sign}
##############################"

        else
            byebye

        fi

        unset mode

    fi

}

# Print message into Log file
_log() {

    text="$1"
    if [ -n "$2" ]; then status="$2"; else status=""; fi
    if [ -f "${bz_log}" ]; then
        case "${status}" in
            "KO"|"OK")
                printf '%s: %s \n' "${status}" "${text}" >> "${bz_log}";
            ;;
             "#")   # manage title
                printf '\n%s \n' "${text}" >> "${bz_log}";
             ;;
            *)
                printf '%s \n' "${text}" >> "${bz_log}";
            ;;
        esac

    fi
    unset text

}
