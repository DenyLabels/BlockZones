########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

transformer() {

    [ "${debug}" -eq 1 ] && _log "${ttl_transformer}" "#"
    [ "${verbose}" -eq 1 ] && _mssg "${ttl_transformer}" "#"

    [ ! -d "${DIR_LISTS}" ] && mkdir "${DIR_LISTS}"

    case "$choice" in

        "badips")
            if [ "${dialog}" -eq 1 ]; then
                transformer_bi4d
            else
                transformer_files
            fi
        ;;
        "blacklists")
            if [ "${dialog}" -eq 1 ]; then
                . "${DIR_INC}/transformer_bl.dialog.ksh"
            else
                . "${DIR_INC}/transformer_bl.ksh"
            fi
            transformer_bl
        ;;

        "bogons") transformer_files ;;

    esac

}

transformer_files() {

    [ "${debug}" -eq 1 ] && _log "${txt_transform}: \\n\\n ${txt_wait_minutes}"
    [ "${verbose}" -eq 1 ] && _mssg "${txt_transform}: \\n\\n ${txt_wait_minutes}" "hb"

    for file in "${FILES[@]}"; do

        [ "${debug}" -eq 1 ] && _log "* ${txt_transform_file}'${file}"
        [ "${verbose}" -eq 1 ] && _mssg "${txt_transform_file}'${file}'" "hb"

        if [ -f "${file}" ]; then

            output="$(basename "${file}")"

            if mv "${file}" "${DIR_LISTS}/${output}"; then
                [ "${debug}" -eq 1 ] && _log "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" "OK"
                _mssg "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" "OK"

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_move_file}'${DIR_LISTS}/${output}'" "KO"
                _mssg "${txt_error_move_file}'${DIR_LISTS}/${output}'" "KO"

            fi

            if [ "${one_checksum_file}" -eq 0 ]; then build_sums && create_sign; fi

            unset output

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_no_file}${file}" "KO"
                _mssg "${txt_error_no_file}'${file}'" "KO"

            fi

    done

}
