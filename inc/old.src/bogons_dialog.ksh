########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#	This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2018/11/11
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bg4d() {

    [ "${debug}" -eq 1 ] && _log "# ${ttl_manager}: ${txt_read_array_bl}" 
    _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_read_array_bl}" 10

    count="${#lists[@]}"
    modulo=$(printf '%s\n' "100/$count"|bc)

    [ "${debug}" -eq 1 ] && _log "# manager: count: $count | modulo: $modulo"

    if [ "${count}" -gt 0 ]; then
    
		(

        for url in "${lists[@]}"; do

			file="$(printf '%s\n' "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			filename="${DIR_DL}/${file}"

            if [ "${debug}" -eq 1 ]; then
				_log "${file}" "file: %s \n"
				_log "${filename}" "filename: %s \n"
			fi
			
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $file
XXX
EOF

            [ "${debug}" -eq 1 ] && _log "${txt_nb_seconds}${seconds}"

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(printf '%s\n' "${timestamp} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then

                    del_file "${filename}"
                    sleep 1
                    download

                fi

            else

                download

            fi
            
            if [ -f "${filename}" ]; then
                
                [ "${debug}" -eq 1 ] && _log "# ${ttl_manager}: ${txt_wait_download_file}${filename}" 
                _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_wait_download_file}${filename}" 10 
				
				purge_files
				
				make_uniq_list
				
				if [ -f "${DIR_LISTS}/${file}" ]; then
					
					build_sums
				
				else
                     [ "${debug}" -eq 1 ] && _log "${txt_error}${txt_file}'${DIR_LISTS}/${file}'${txt_error_not_build}" "KO"
                     _dialog "mssg" "${ttl_project_name}" "${ttl_manager}" "${txt_dlg_ko}${txt_error}${txt_file}'${DIR_LISTS}/${file}'${txt_error_not_build}" 10 
				
				fi
				
            fi 

            unset filename

        done
        
        ) | _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_wait}" 10 

    else
		[ "${debug}" -eq 1 ] && _log "${txt_error_no_data}" "KO"
        _dialog "mssg" "${ttl_project_name}" "${ttl_manager}" "${txt_dlg_ko}${txt_error_no_data}"
        sleep 1
        
        byebye

    fi

    unset count

}
