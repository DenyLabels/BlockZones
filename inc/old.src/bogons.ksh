########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#	This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/01/03
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bg() {

    [ "${debug}" -eq 1 ] && _log "## ${txt_read_array_bl}"
    [ "${verbose}" -eq 1 ] && _mssg "${txt_read_array_bl}" "#"

    count="${#lists[@]}"

    if [ "${count}" -gt 0 ]; then

        for url in "${lists[@]}"; do

			if [[ "${url}" == *"bz_bogons"* ]]; then
				file=""
				filename="${DIR_SRC}/${url}"
			else
				file="$(printf '%s\n' "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
				filename="${DIR_DL}/${file}"
			fi

            if [ "${debug}" -eq 1 ]; then
				_log "# ${txt_file}${file}" 
				_log "# ${txt_filename}${filename}" 
                _log "# ${txt_nb_seconds}${seconds}" 
			fi

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(printf '%s\n' "${timestamp} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then

                    if [[ "${url}" != *"bz_bogons"* ]]; then 
                    
                        del_file "${filename}"
                        sleep 1
                        download
                        
                    fi

                fi

            else

                if [[ "${url}" != *"bz_bogons"* ]]; then download; fi

            fi
            
            if [ -f "${filename}" ]; then
				
				purge_files
				
				make_uniq_list
				
            fi 

            unset filename

        done

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_no_data}" "KO"
        _mssg "${txt_error_no_data}" "KO"
        
        byebye

    fi

    unset count

}
