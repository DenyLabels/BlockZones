########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#	This file is part of "DenyLabels :: BlockZones Project"
#
# Date: 2018/11/11
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bl4d() {

	[ "${debug}" -eq 1 ] && _log "# ${ttl_manager}: ${txt_read_array_bl}" 
    _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_read_array_bl}" 10

    count="${#lists[@]}"
    modulo=$(echo "100/$count"|bc)
    
    [ "${debug}" -eq 1 ] && _log "# manager: count: $count | modulo: $modulo"

    if [ "${count}" -gt 0 ]; then
    
		(

        for url in "${lists[@]}"; do

			if [ "${url}" = "personals" ]; then
				filename="${DIR_SRC}/${url}"
			else
				ndd="$(printf '%s\n' "${url}" | awk -F'/' '{ print $3 }')"
				file="$(printf '%s\n' "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
				name="${ndd}_${file}"
				filename="${DIR_DL}/${name}"
			fi
			
            if [ "${debug}" -eq 1 ]; then
				_log"${ndd}" "domain name: %s \n" 
				_log "${file}" "file: %s \n"
				_log "${filename}" "filename: %s \n"
			fi
            
            let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $name
XXX
EOF

            # define seconds before new dl
            case "${ndd}" in
                "mirror1.malwaredomains.com") seconds=2592000;;   # 1 month
                "winhelp2002.mvps.org") seconds=604800;; # 7 days
                "www.spamhaus.org") seconds=3600;; # 1 hours
                #*) seconds=86400;;
            esac
            
            [ "${debug}" -eq 1 ] && _log "${seconds}" "nb seconds: %s \n"

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(printf '%s\n' "${timestamp} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then 
                
					if [ "${url}" != "personals" ]; then 
                        
                        del_file "${filename}"
                        sleep 1
                        download
                        
                    fi
				
				fi

            else

                if [ "${url}" != "personals" ]; then download; fi

            fi

            if [ -f "${filename}" ]; then
                get_mimes
                uncompress

                case "${ndd}" in
                    "hosts-file.net")
                        if [ "${mimetype}" = "application/zip" ]; then
                            filename="${filename%.zip}/hosts.txt"
                        fi
                    ;;
                    "mirror1.malwaredomains.com")
                        if [ "${mimetype}" = "application/zip" ]; then
                            case "${file}" in
                                "immortal_domains.zip")
                                    filename="${filename%.zip}/${file%.zip}.txt"
                                ;;
                                "justdomains.zip"|"malwaredomains.zones.zip")
                                    filename="${filename%.zip}/${file%.zip}"
                                ;;
                            esac
                        fi
                    ;;
                    "winhelp2002.mvps.org")
                        if [ "${mimetype}" = "application/zip" ]; then
                            filename="${filename%.zip}/HOSTS"
                        fi
                    ;;
                esac

                purge_files

                make_uniq_list

                unset filename
                
            fi

        done
        
        ) | _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_wait}" 10 

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_no_data}" "KO"
        _dialog "mssg" "${ttl_project_name}" "${ttl_manager}" "${txt_dlg_ko}${txt_error_no_data}"
                
        sleep 1
        
        byebye

    fi

    unset count

}


transformer_bl4d() {

	[ "${debug}" -eq 1 ] && _log "# ${ttl_transformer}: ${txt_transform_file}'${FILES[0]}'${txt_in_list}:'${choice_bl}'...\n\n ${txt_wait_minutes}"
    _dialog "info" "${ttl_project_name}" "${ttl_transformer}" "${txt_transform_file}'${FILES[0]}':'${choice_bl}' \n\n ${txt_wait_minutes}" 10 
    
    case "${choice_bl}" in
        "bind"|"bind8"|"bind9")
            format="Bind Config"
            output="bind.zone"
        ;;
        "host"|"hosts"|"host0")
            format="hosts"
            output="hosts"
        ;;
        "unbound")
            format="Local zone"
            output="local-zone"
        ;;
        "pf")
			format="Packet-Filter"
			output="baddomains"
        ;;
    esac

    if [ -f "${FILES[0]}" ];  then

        mssg="###########################
### ${ttl_project_name} ###
###########################
### ${txt_format}'${format}'
### ${txt_data}${now}
##
#"

        _out "${mssg}"
        unset mssg
        
        case "${choice_bl}" in
			"host0")
                _out "0.0.0.0 localhost"
             ;;
             "host"|"hosts")
				if [ "${IPv4}" = 1 ]; then _out "127.0.0.1 localhost"; fi
				if [ "${IPv6}" = 1 ]; then _out "::1 localhost"; fi
             ;;
        esac
        
        # add any lines that do not start with #, 127.0.0.1, ::1 of the hosts file into output file
        #case "${choice_bl}" in
            #"host"|"hosts"|"host0")
                #grep -Ev "^#|127.0.0.1|::1" /etc/hosts >> "${DIR_LISTS}/${output}"
            #;;
        #esac

        i=0
        while read -r line; do
            # replace '[:space:]' by '\040\011\012\015' for oldier version
            line="$(printf '%s\n' "${line}" | tr -d '[:space:]')"    
            #line="$(echo "${line}" | tr -d '[:space:]')"    

            case "${choice_bl}" in
                "bind8")
                    _out "zone \"${line}\" { type master; notify no; file \"null.zone.file\"; };"
                ;;
                "bind"|"bind9")
                    _out "zone \"${line}\" { type master; notify no; file \"/etc/bind/nullzonefile.txt\"; };"
                ;;
                "host0")
                    _out "0.0.0.0 ${line}"
                ;;
                "host"|"hosts")
                    if [ "${IPv4}" = 1 ]; then _out "127.0.0.1 ${line}"; fi
                    if [ "${IPv6}" = 1 ]; then _out "::1 ${line}"; fi
                ;;
                "pf")
					_out "${line}"
                ;;
                "unbound")
                    if [ "${IPv4}" = 1 ]; then 
						if [ "${USE_LZ_REDIRECT}" = 1 ]; then
							_out "local-zone: \"${line}\" redirect\nlocal-data: \"${line} A 127.0.0.1\""
							#echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}"; 
						else
							_out "local-data: \"${line} A 127.0.0.1\""
							#echo -e "local-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}";
						fi
					fi
                    if [ "${IPv6}" = 1 ]; then 
						if [ "${USE_LZ_REDIRECT}" = 1 ]; then
							_out "local-zone: \"${line}\" redirect\nlocal-data: \"${line} AAAA ::1\""
							#echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}"; 
						else
							_out "local-data: \"${line} AAAA ::1\""
							#echo -e "local-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}"; 
						fi
					fi
                ;;
            esac

            #let i++
            (( i++ ))
        done  < "${FILES[0]}"
        unset i
        
        del_file "${FILES[0]}"

    else
		[ "${debug}" -eq 1 ] && _log "${txt_error_no_file}${FILES[0]}" "KO"
        _dialog "mssg" "${ttl_project_name}" "${ttl_transformer}" "${txt_dlg_ko}${txt_error_no_file}'${FILES[0]}'"
        sleep 1
        
        byebye

    fi

    unset arg

    if [ -f "${DIR_LISTS}/${output}" ]; then
		[ "${debug}" -eq 1 ] && _log "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" "OK" 
        _dialog "info" "${ttl_project_name}" "${ttl_transformer}" "${txt_dlg_ok}${txt_file}'${DIR_LISTS}/${output}'${txt_builded}"
		sleep 1

        build_sums

    else
		[ "${debug}" -eq 1 ] && _log "${txt_error_create_checksum_file}${DIR_LISTS}/${output}" "KO"
		_dialog "mssg" "${ttl_project_name}" "${ttl_transformer}" "${txt_dlg_ko}${txt_error_create_checksum_file}${DIR_LISTS}/${output}"
		sleep 1
		
        byebye

    fi

}
