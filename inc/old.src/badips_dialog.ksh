########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#	This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/01/02
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bi4d() {
	
	[ "${debug}" -eq 1 ] && _log "# ${ttl_manager}: ${txt_read_array_bl}" 
    _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_read_array_bl}" 10

    count="${#lists[@]}" 
	modulo=$(printf '%s\n' "100/$count"|bc)
    
    [ "${debug}" -eq 1 ] && _log "# manager: count: $count | modulo: $modulo"

    if [ "${count}" -gt 0 ]; then
    
    	(
				
		#let counter+=$modulo
		#[ $counter -ge 100 ] && break

		for url in "${lists[@]}"; do

			ndd="$(printf '%s\n' "${url}" | awk -F'/' '{ print $3 }')"
			file="$(printf '%s\n' "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			name="${ndd}_${file}"
			filename="${DIR_DL}/${name}"
			
			if [ "${debug}" -eq 1 ]; then
				_log"${ndd}" "domain name: %s \n" 
				_log "${file}" "file: %s \n"
				_log "${filename}" "filename: %s \n"
			fi
				
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $name
XXX
EOF

			# define seconds before new dl
			case "${ndd}" in
				"feeds.dshield.org") seconds=259200 ;;	# 3 days
				"lists.blocklist.de") seconds=172800 ;;	# 2 days
				"myip.ms") seconds=864000 ;;	# 10 days
				"ransomwaretracker.abuse.ch") seconds=2592000 ;;	# 30 days
				"osint.bambenekconsulting.com") seconds=600 ;; # 10 min.
				#"sslbl.abuse.ch") seconds=900 ;; # 15 min.
				#"www.openbl.org") seconds=172800 ;;	# 2 days
				"www.spamhaus.org") seconds=3600;; # 1 hours
				#*) seconds=86400;;
			esac
            
            [ "${debug}" -eq 1 ] && _log "${txt_nb_seconds}${seconds}"

			if [ -f "${filename}" ]; then

				# get file seconds stat
				file_seconds=$(stat -f "%m" -t "%s" "${filename}")

				# calcul diff time in seconds
                diff_sec=$(printf '%s\n' "${timestamp} - ${file_seconds}" | bc)

				#unset file_seconds

				if [ ${diff_sec} -gt ${seconds} ]; then 
                
                    del_file "${filename}"
                    sleep 1
                    download
                
                fi

			else

				download

			fi

            if [ -f "${filename}" ]; then
            
                case "${ndd}" in
                    "www.openbl.org")
                        uncompress
				
                        filename="${filename%.gz}"
                    ;;
                esac
			
                purge_files

                make_uniq_list

                unset filename name ndd file
                
            fi
            
		done
			
		) | _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_wait}" 10 

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_no_data}" "KO"
        _dialog "mssg" "${ttl_project_name}" "${ttl_manager}" "${txt_dlg_ko}${txt_error_no_data}"
        sleep 1
        
        byebye

    fi
    
    sleep 1

    unset count

}

transformer_bi4d() {
	
	#[ "${debug}" -eq 1 ] && printf "%s \n" "# ${ttl_transformer}: ${txt_transform_file}...\n\n ${txt_wait_minutes}" >> "${bz_log}"
	#dialog --backtitle "${ttl_project_name}" --title "${ttl_transformer}" --infobox "${txt_transform_file} \n\n ${txt_wait_minutes}" 10 100 
    [ "${debug}" -eq 1 ] && _log "# ${ttl_transformer}: ${txt_transform_file}'${FILES[0]}'${txt_in_list}:'${choice_bl}'...\n\n ${txt_wait_minutes}"
    _dialog "info" "${ttl_project_name}" "${ttl_transformer}" "${txt_transform_file}'${FILES[0]}':'${choice_bl}' \n\n ${txt_wait_minutes}" 10 
	
	count="${#FILES[@]}"
	modulo=$(printf '%s\n' "100/$count"|bc)
	#[ "${debug}" -eq 1 ] && printf "%s %s | %s \n" "# transformer:" "count: $count" "modulo: $modulo" >> "${bz_log}"
	[ "${debug}" -eq 1 ] && _log "# transformer: count: $count; modulo: $modulo"
	
	(
	
	for file in "${FILES[@]}"; do
		
		if [ -f "${file}" ]; then
		
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_transform $list ($x%)
$txt_file $file
XXX
EOF
			
			output="$(basename "${file}")"

			if mv "${file}" "${DIR_LISTS}/${output}"; then 
				#[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" >> "${bz_log}"
				#display_mssg "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}"
                
                [ "${debug}" -eq 1 ] && _log "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" "OK"
                _dialog "info" "${ttl_project_name}" "${ttl_transformer}" "}" "${txt_dlg_ok}${txt_file}'${DIR_LISTS}/${output}'${txt_builded}"
			
			else
				#[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'" >> "${bz_log}"
				#display_mssg "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'"
                
                [ "${debug}" -eq 1 ] && _log "${txt_error_move_file}'${DIR_LISTS}/${output}'" "K0"
                _dialog "mssg" "${ttl_project_name}" "${ttl_transformer}" "}" "${txt_dlg_ko}${txt_error_move_file}'${DIR_LISTS}/${output}'"
			
			fi
			
			build_sums
			
			unset output
			
		else
			#[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_no_file}${f}" >> "${bz_log}"
			#dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_transformer}" --infobox "${txt_dlg_ko}${txt_error_no_file}${f}" 10 100
            
            [ "${debug}" -eq 1 ] && _log "${txt_error_no_file}'${file}' " "K0"
            _dialog "mssg" "${ttl_project_name}" "${ttl_transformer}" "${txt_dlg_ko}${txt_error_no_file}'${file}' " 10 
		
		fi
		
		
    done
    ) | _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_wait}" 10 

	sleep 1
	
}
