########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

. "${DIR_INC}/commons_functions.ksh"

_welcome

if [ "${menu}" = "menu" ]; then

    [ "$dialog" -eq 1 ] && menu_dialog || menu

fi

########################################################################
###
##
#    EXECUTION: DO NOT TOUCH!
##
###
########################################################################

del_files

build_lists

mng_lists

[ "${choice}" = "blacklists" ] && build_uniq_list

transformer

create_one_sums

install_service

_mssg_end
