########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/01/17
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#   Save yours preferences into a new file 'inc/cfg_vars.local' ;)
##
###
########################################################################

### Use colors messages; disabled if cron=1
# If '0': disabled
# If '1': enabled; by default
typeset -i use_color=1

### to get checksums files into on file.
# If '0', one checksum file by list
# If '1', one chechsum file for all lists; by default
typeset -i one_checksum_file=1

### to use tools to download
typeset -i use_curl=0
typeset -i use_wget=0

### to use signify tool
# If '0': disabled
# If '1': enabled; by default
typeset -i use_sign=1

### modes: modify with precautions!
typeset -i cron=0       # set if using cron task... to display minimal messages.
typeset -i debug=0      # set debug mode
typeset -i dialog=0     # set dialog mode
typeset -i verbose=0    # set verbose mode

### using timestamp or day date; useful for logname
# If '0': day date
# If '1': timestamp; by default
typeset -i use_timestamp=1

### copy list in the folder config for services
# If '0': disabled
# If '1': enabled; by default
typeset -i install_list=0
typeset -i use_hosts=0  # to install /etc/hosts
set -A pf_tables 'badips' 'badips6' 'bogons' 'bogons6'   # tables for PF rules

### unbound
# enable or not to use local-zone redirect; modify only if you know!
# If '0': disabled; by default
# If '1': enabled
typeset -i USE_LZ_REDIRECT=0
# enable or not IPv{4,6} management; useful too for hosts...
# If '0': disabled
# If '1': enabled; by default
typeset -i IPv4=1
typeset -i IPv6=1
