########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

check_installed_services() {

    [ "${debug}" -eq 1 ] && _log "${ttl_services}" "#"
    [ "${verbose}" -eq 1 ] && _mssg "${ttl_services}" "#"

    if [ $(which bind 2>/dev/null) ]; then
        [ "${debug}" -eq 1 ] && _log "${txt_use_service} bind"
        #_mssg "${txt_use_service} bind" "hg"
        use_bind=1
    fi

    if [ $(which unbound 2>/dev/null) ]; then
        [ "${debug}" -eq 1 ] && _log "${txt_use_service} unbound"
        #_mssg "${txt_use_service} unbound" "hg"
        if [[ "$(rcctl check unbound)" == *"ok"* ]]; then use_unbound=1; fi
    fi
    
    if [ $(which unwind 2>/dev/null) ]; then
        [ "${debug}" -eq 1 ] && _log "${txt_use_service} unwind"
        #_mssg "${txt_use_service} unbound" "hg"
        if [[ "$(rcctl check unwind)" == *"ok"* ]]; then use_unwind=1; fi
    fi

}

install_etc() {

    for file in "${FILES[@]}"; do
        output="$(basename "${file}")"

        if [ -f "${DIR_LISTS}/${output}" ]; then
            [ ! -d "${dir_dest_bz}" ] && mkdir -p "${dir_dest_bz}"
            cp "${DIR_LISTS}/${output}" "${dir_dest_bz}/"
            chmod 0600 "${DIR_LISTS}/${output}"
        fi

        if [ -f "${dir_dest_bz}/${output}" ]; then
            case "${output}" in
                "badips_ipv4") table="${pf_tables[0]}" ;;
                "badips_ipv6") table="${pf_tables[1]}" ;;
                "bogons_ipv4") table="${pf_tables[2]}" ;;
                "bogons_ipv6") table="${pf_tables[3]}" ;;
            esac
            restart_pf_tables
        fi

    done

}

install_service() {

    if [ "${install_list}" -eq 1 ]; then
        [ "${debug}" -eq 1 ] && _log "${ttl_installer}" "#"
        [ "${verbose}" -eq 1 ] && _mssg "${ttl_installer}" "#"

        case "${choice}" in
            "badips"|"bogons")
                install_etc
            ;;
            "blacklists")
                cp_for_blacklists
                restart_service
            ;;
        esac

    else
        [ "${debug}" -eq 1 ] && _log "${txt_not_install_list}"
        _mssg "${txt_not_install_list}" "OK"

    fi

}

restart_pf_tables() {

    pfctl -t $table -T replace -f "${dir_dest_bz}/${output}"

}

restart_service() {
    
    case "${OSN}" in
        "OpenBSD")
            if [ "${use_unbound}" -eq 1 ]; then
                if unbound-checkconf; then rcctl restart unbound; fi
            fi
    
            if [ "${use_unwind}" -eq 1 ]; then
                if unwind -n; then rcctl restart unwind; fi
            fi
        ;;
    esac

}
