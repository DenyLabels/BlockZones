########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### Downloader needed files
download() {

    [ "${debug}" -eq 1 ] && _log "${ttl_download}" "#"
    if [ "${verbose}" -eq 1 ]; then _mssg "${ttl_download}" "#"; _mssg "${txt_download_file}${url}" "hb"; fi

    typeset bool=0 options=""

    [ ! -d "${DIR_DL}" ] && mkdir "${DIR_DL}"

    if [ "${use_curl}" -eq 1 ]; then

        if [ "${dialog}" -eq 1 ]; then options="-s "; fi

        if [ "${verbose}" -eq 1 ]; then options="-v "; fi

        if [ "${debug}" -eq 1 ]; then
            options='-v -w "%{response_code};%{time_total}" '
            piper /usr/local/bin/curl -A "${UA}" --compressed ${options} -o "${filename}" "${url}" \| tee >> "${bz_log}" 2>&1

            if [ "$pipestatus_1" -ne 0 ]; then bool=1; fi

        else
            /usr/local/bin/curl -A "${UA}" --compressed ${options} -o "${filename}" "${url}"
            status=$?
            if [ "$status" -ne 0 ]; then bool=1; fi
            unset status

        fi

    elif [ "${use_wget}" -eq 1 ]; then

        if [ "${dialog}" -eq 1 ]; then options+="-q "; fi

        if [ "${debug}" -eq 1 ]; then
            options+="-d "
            piper /usr/local/bin/wget --user-agent="${UA}" -c "${options}" -O "${filename}" "${url}" \| tee >> "${bz_log}" 2>&1

            if [ "$pipestatus_1" -ne 0 ]; then bool=1; fi

        else
            /usr/local/bin/wget --user-agent="${UA}" -c "${options}" -O "${filename}" "${url}"
             status=$?
            if [ "$status" -ne 0 ]; then bool=1; fi
            unset status

        fi

    else

        options="-mn"

        if [ "${dialog}" -eq 1 ]; then options="-nV"; fi

        if [ "${debug}" -eq 1 ]; then
            #options="-dmn -C "
            #piper /usr/bin/ftp -C -d ${options} -o "${filename}" "${url}" 2>&1 \| tee >> "${bz_log}"
            /usr/bin/ftp -C -d ${options} -o "${filename}" "${url}" >> "${bz_log}" 2>&1
            status=$?
            if [ "$status" -ne 0 ]; then bool=1; fi
            unset status

        else
            /usr/bin/ftp -C ${options} -o "${filename}" "${url}"
            status=$?
            if [ "$status" -ne 0 ]; then bool=1; fi
            unset status

        fi

    fi

    if [ "${bool}" -eq 0 ]; then
        [ "${debug}" -eq 1 ] && _log "${txt_file}${filename}${txt_downloaded}" "OK"
        [ "${verbose}" -eq 1 ] && _mssg "${txt_file}${filename}${txt_downloaded}" "OK"

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_download_file}${filename}!" "KO"
        _mssg "${txt_error_download_file}${filename}!" "KO"
        #byebye

    fi

    unset bool options

}
