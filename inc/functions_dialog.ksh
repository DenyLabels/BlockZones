########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2018/11/19
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

_dialog() {

    type="$1"  backtitle="$2"; title="$3"; mssg="$4"
    if [ -z "$5" ]; then height=7; else height="$5"; fi

    case "${type}" in
        "cls")
            dialog --clear
        ;;
        "info")
            dialog --colors --backtitle "${backtitle}" --title "${title}" --infobox "${mssg}" ${height} 100
        ;;
        "mssg")
            dialog --colors --backtitle "${backtitle}" --title "${title}" --msgbox "${mssg}" ${height} 100
        ;;
        "yn")
            dialog --colors --backtitle "${backtitle}" --title "${title}" --yesno "${mssg}" ${height} 100
        ;;
    esac

    unset backtitle mssg title type

}

menu_dialog() {

    if [ -z "$1" ]; then
        set -A dialog_menu
        INPUT="/tmp/BZ_menu.sh.$$"

        get_array_menu

        dialog --clear --backtitle "${ttl_project_name}" --title "${ttl_menu}" --menu "${txt_dialog_menu}" 0 0 10 "${dialog_menu[@]}" 2> "${INPUT}"

        return="$?"
        choice="$(tr [:upper:] [:lower:] < "${INPUT}")"
        [ "${debug}" -eq 1 ] && _log "*** menu: $choice"

        [ -f "$INPUT" ] && del_file "${INPUT}" "p"
        unset dialog_menu INPUT

        case "${return}" in
            1) _mssg_end ;;
        esac

        if [ -z "${choice}" ]; then menu_dialog; fi
    else
        menu="$1"
    fi

    case "${choice}" in

        "blacklists")

            set -A dialog_menu
            INPUT="/tmp/BZ_BL_menu.sh.$$"

            get_array_menu

            dialog --clear --backtitle "${ttl_project_name}" --title "${ttl_menu_blacklist}" --menu "${txt_dialog_menu}" 0 0 10 "${dialog_menu[@]}" 2> "${INPUT}"

            return="$?"
            blacklist="$(tr [:upper:] [:lower:] < "${INPUT}")"
            [ "${debug}" -eq 1 ] && _log "*** menu: $choice | blacklist: $blacklist"

            case "${return}" in
                1)
                    if _dialog "yn" "${ttl_project_name}" "${ttl_menu_blacklist}" "${txt_answer_sure}" 10; then
                        _mssg_end
                    else
                        menu_dialog "${choice}"
                    fi
                ;;
            esac

            [ -f "$INPUT" ] && del_file "$INPUT" "p"
            unset dialog_menu INPUT
        ;;

        "quit") _mssg_end ;;

    esac

    setVariables

}

_mssg_end() {

    [ "${debug}" -eq 1 ] && _log "### ${txt_goodbye} :: ${txt_ended} ###" "#"

    _dialog "mssg" "${ttl_project_name}" "${ttl_project_name}" "${txt_dlg_goodbye}" 10

    _dialog "cls"
    clear

    exit 0

}

_welcome() {

    if _dialog "yn" "${ttl_project_name}" "${ttl_project_name}" "${txt_dlg_welcome}" 10; then
        sleep 1

    else
        _mssg_end

    fi

}
