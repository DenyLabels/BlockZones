########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################
### Ty to @kuniyoshi for FreeBSD Codes - 2017/07/10
### Ty to @ArRay_, @kilobug - 2018/11
########################################################################

# Get languages texts
if [ -f "${DIR_LANG}/texts.${lang}" ]; then . "${DIR_LANG}/texts.${lang}"; else . "${DIR_LANG}/texts.en"; fi
if [ -f "${DIR_LANG}/titles.${lang}" ]; then . "${DIR_LANG}/titles.${lang}"; else . "${DIR_LANG}/titles.en"; fi

########################################################################

byebye() {

    [ "${debug}" -eq 1 ] && _log "${txt_stop} \\n\\n ${txt_search_reason}" "KO"

    if [ -x /usr/local/bin/dialog ]; then
        _dialog "mssg" "${ttl_project_name}" "${title_byebye}" "${txt_dlg_ko}${txt_stop} \\n\\n ${txt_search_reason}" 10

    else
        _mssg "${txt_stop}" "KO"
        _mssg "${txt_search_reason}" "KO"

    fi

    exit 1;

}

########################################################################

confirm () {

    read -r response?"${1}${txt_yn}"
    case "$response" in
        # O is not zero: O(ui) ;)
        y|Y|o|O|1)  true ;;
        *)          false ;;
    esac

}
#######################################################################

help() {

    _mssg "${txt_help_usage} $0 ${txt_options}"
    _mssg "${txt_help_options}"
    _mssg " -b: ${txt_help_opt_b}"
    _mssg " -h: ${txt_help_opt_h}"
    _mssg " -i: ${txt_help_opt_i}"
    _mssg " -l: ${txt_help_opt_l}"
    _mssg " -m: ${txt_help_opt_m}"

}

########################################################################

_mssg() {

    typeset color info status
    info="$1"
    if [ -n "$2" ]; then status="$2"; else status=""; fi

    case "${status}" in
        "KO"|1) color="${red}"      ;;
        "OK"|0) color="${green}"    ;;
    esac

    if [ -n "${status}" ]; then
        case "${status}" in
            "#")    # manage title
                if [ "${OSN}" = "FreeBSD" ] && [ "${use_color}" -eq 1 ]; then
                    printf '%s \n' "[ "; tput bold; printf "%s" "${info}"; tput sgr0; printf "%s" "]";

                else
                    printf "[ ${bold}%s${neutral} ] \\n" "${info}"

                fi
            ;;
            "hb")
                if [ "${OSN}" = "FreeBSD" ] && [ "${use_color}" -eq 1 ]; then
                    tput dim; printf '%s \n' "${info}"; tput sgr0; printf '\n'

                else
                    printf "${dim}%s${neutral} \\n" "${info}"

                fi
            ;;
            "ln")
                printf '\n%s \n' "${info}"
            ;;
            *)
                if [ "${OSN}" = "FreeBSD" ] && [ "${use_color}" -eq 1 ]; then
                    case "${status}" in
                        "KO")
                            printf "%s" "[ "; tput setaf 1; printf "%s" "KO"; tput sgr0; printf '%s \n' " ] ${info}"
                        ;;
                        "OK")
                            printf "%s" "[ "; tput setaf 2; printf "%s" "OK"; tput sgr0; printf '%s \n' " ] ${info}"
                        ;;
                    esac

                else
                    printf "[ ${color}%s${neutral} ] %s \\n" "${status}" "${info}"

                fi
            ;;
        esac

    else
        printf '%s \n' "${info}"

    fi

    unset color info statut text

}

########################################################################

_out() {

    text="$1"
    printf '%s\n' "${text}" >> "${DIR_LISTS}/${output}"
    unset text

}

########################################################################
. "${DIR_INC}/arrays.ksh"
. "${DIR_INC}/checker.ksh"
. "${DIR_INC}/downloader.ksh"
. "${DIR_INC}/extract_archives.ksh"
. "${DIR_INC}/mimic_pipe.ksh"
. "${DIR_INC}/mng_bl.ksh"
. "${DIR_INC}/mng_files.ksh"
. "${DIR_INC}/mng_lists.ksh"
. "${DIR_INC}/mng_log.ksh"
. "${DIR_INC}/mng_menu.ksh"
. "${DIR_INC}/mng_services.ksh"
. "${DIR_INC}/mng_sums.ksh"
. "${DIR_INC}/purge.ksh"
. "${DIR_INC}/set_variables.ksh"
. "${DIR_INC}/transformer.ksh"
. "${DIR_INC}/valid_ips.ksh"
########################################################################

while getopts ":bhil:m" opt; do

    case "${opt}" in

        b)  menu="bogons"   ;;
        h)  help; exit    ;;
        i)  menu="badips"   ;;
        l)
            menu="blacklists"
            blacklist="unbound"
            if [ -n "$OPTARG" ]; then blacklist="$OPTARG"; fi
        ;;
        m)  menu="menu" ;;

        :)
            if [ "$OPTARG" = "l" ]; then
                menu="blacklists"
                blacklist="unbound"
            fi
        ;;

        \?)
            _mssg "${txt_error_option}$OPTARG" "KO"
            help
            byebye
        ;;

    esac

done
unset opt

[ -z "${menu}" ] && { help; exit 1; }

if in_array "${menu}" "${menus[@]}"; then

    in_array "${blacklist}" "${menus_blacklists[@]}"
    status="$?"
    if [ "${status}" -eq 1 ]; then 
        _mssg "${txt_error_not_in_menu_bl}" "KO"
        help; exit; 
    fi
    
else
    _mssg "${txt_error_not_in_menu}" "KO"
    help; exit; 
fi

setLogs
[ "${menu}" != "menu" ] && setVariables
is_install_list
check_installed_services
check_needed_softs
