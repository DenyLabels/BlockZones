########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### To delete files
del_file() {

    bool=1
    file="$1"
    if [ -n "$2" ] && [ "$2" = "p" ]; then
        if [ -f "${file}" ]; then rm -fP "${file}" && bool=0; fi
    else
        if [ -f "${file}" ]; then rm "${file}" && bool=0; fi
    fi

    if [ "${bool}" -eq 0 ];  then
        [ "${debug}" -eq 1 ] && _log "${txt_file}${file}${txt_deleted}" "OK"
        [ "${verbose}" -eq 1 ] && _mssg "${txt_file}${file}${txt_deleted}" "OK"
        return 0

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_del_file}${file}" "KO"
        _mssg "${txt_error_del_file}${file}" "KO"
        return 1

    fi

    unset bool file

}

del_files() {

    [ "${debug}" -eq 1 ] && _log "${ttl_del_files} " "#"
    [ "${verbose}" -eq 1 ] && _mssg "${ttl_del_files}" "#"

    for file in "${FILES[@]}"; do del_file "${file}" "p"; done

    del_sums

}

### To delete spaces, lines into files
del_spaces() {

    [ "${debug}" -eq 1 ] && _log "${ttl_del_spaces}" "#"
    [ "${verbose}" -eq 1 ] && _mssg "${ttl_del_spaces}" "hb"

    # delete empties spaces and lines...
    for file in "${FILES[@]}"; do

        { del_file "${file}" && sed -e "${p_blank}" | sort -u -o "${file}"; } < "${file}"

    done

}

get_mimes() {

    [ -f "${filename}" ] && mimetype="$(file -b -i "${filename}")"

}

### To created files
touch_files() {

    for file in "${FILES[@]}"; do

        if [ ! -f "${file}" ]; then

            [ "${debug}" -eq 1 ] && _log "${ttl_touch_files}${file}"  "#"
            [ "${verbose}" -eq 1 ] && _mssg "${ttl_touch_files}${file}" "#"

            if touch "${file}"; then
                [ "${debug}" -eq 1 ] && _log "${txt_file}'${file}'${txt_created}" "OK"
                _mssg "${txt_file}'${file}'${txt_created}" "OK"

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_create_file}'${file}' " "KO"
                _mssg "${txt_error_create_file}'${file}'" "KO"

            fi

        fi

    done

}
