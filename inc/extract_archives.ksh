########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### extract archive
uncompress() {

    case "${mimetype}" in
        "application/gzip"|"application/x-gzip")
            [ "${debug}" -eq 1 ] && _log "${txt_xtract_gz}${filename}"
            [ "${verbose}" -eq 1 ] && _mssg "${txt_xtract_gz}${filename}" "hb"
            /usr/bin/gunzip -d -f -q "${filename}";
        ;;
        "application/zip")
            [ "${debug}" -eq 1 ] && _log "${txt_xtract_zip}${filename}"
            [ "${verbose}" -eq 1 ] && _mssg "${txt_xtract_zip}${filename}" "hb"
            /usr/local/bin/unzip -oqu "${filename}" -d "${filename%.zip}"
        ;;
    esac

}
