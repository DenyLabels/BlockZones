########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/05/29
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

array_search() {
    
    local i=0 need="$1" IFS=" "; shift; set -A array -- "$@"
    count="${#array[@]}"
    while [ $i -ne $count ]; do
        if [ "${array[$i]}" = "${need}" ]; then echo "$i"; fi
        let "i=$i+1"
    done
    return 1
    unset i need IFS array

}

in_array() {
    
    local i=0 need="$1" IFS=" "; shift; set -A array -- "$@"
    count="${#array[@]}"
    while [ $i -le $count ]; do
        if [ "${array[$i]}" = "${need}" ]; then return 0; fi # true
        let "i=$i+1"
    done
    return 1
    unset i need IFS array

}
