########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

check_needed_softs() {

    [ "${debug}" -eq 1 ] && _log "${ttl_softs}" "#"
    [ "${verbose}" -eq 1 ] && _mssg "${ttl_softs}" "#"

    # dialog
    if [ "${dialog}" -ge 1 ]; then

        if [ ! -x /usr/local/bin/dialog ]; then
            [ "${debug}" -eq 1 ] && _log "${txt_need_dialog}" "KO"
            _mssg "${txt_need_dialog}" "KO"
            byebye

        else
            . "${DIR_INC}/functions_dialog.ksh"

        fi

    else
        . "${DIR_INC}/functions.ksh"

    fi

    ### test if installed tools downloader
    ## curl
    if [ $use_curl -ge 1 ]; then
        if [ -x /usr/local/bin/curl ] ; then
             [ "${debug}" -ge 1 ] && _log "${txt_use_tool} curl"
            _mssg "${txt_use_tool} curl" "hg"
            dldr='curl'
            use_curl=1

        else
            [ "$cron" -eq 0 ] && _mssg "${txt_need_curl}" "hg"
            sleep 1

        fi
    fi

    ## wget
    if [ $use_wget -ge 1 ]; then
        if [ -f /usr/local/bin/wget ]; then
             [ "${debug}" -eq 1 ] && _log "${txt_use_tool} wget"
            _mssg "${txt_use_tool} wget" "hg"
            dldr='wget'
            use_wget=1

        else
            [ "$cron" -eq 0 ] && _mssg "${txt_need_wget}" "hg"
            sleep 1

        fi
    fi

    ## if not curl and not wget use ftp by default
    if [ $use_curl -eq 0 ] && [ $use_wget -eq 0 ]; then
         [ "${debug}" -eq 1 ] && _log "${txt_use_tool} ftp"
        _mssg "${txt_use_tool} ftp" "hg"
    fi

    # signify
    if [ "${OSN}" = "FreeBSD" ]; then
        if [ ! -f /usr/local/bin/signify ]; then
            use_sign=0

            [ "${debug}" -ge 1 ] && _log "${txt_error}${txt_error_signify}" "KO"
            _mssg "${txt_error_signify}" "KO"

            if confirm "${txt_answer_without_signify}"; then
                _mssg "${txt_without_signify}" "#"
                sleep 1

            else
                _mssg "${txt_plz_install_tool}signify" "#"
                byebye

            fi
        fi
    fi

    # unzip
    if [ ! -f /usr/local/bin/unzip ]; then
         [ "${debug}" -ge 1 ] && _log "${txt_error}${txt_error_unzip_tool}" "KO"
        _mssg "${txt_error_unzip_tool}" "KO"
        _mssg "${txt_plz_install_tool}unzip" "#"
        byebye
    fi

}

detect_uid() {

    if [ "$(id -u)" -ne 0 ]; then

        if [ "${install_list}" -eq 1 ]; then
            [ "${debug}" -eq 1 ] && _log "*** ${txt_need_admin_rights}${txt_option_install_list}${install_list}"
            _mssg "${txt_option_install_list}${install_list}" "KO"
        fi

        _mssg "${txt_need_admin_rights}" "KO"
        byebye

    fi

}
