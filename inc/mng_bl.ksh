########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

cp_for_blacklists() {

    dir_dest=""
    file_bckp=""
    install=0

    case "${output}" in

        "bind.zone")
            dir_dest="${dir_dest_bind}"
            [ "${use_bind}" -eq 1 ] && install=1
        ;;

        "hosts")
            dir_dest="${dir_dest_host}"
            [ "${use_hosts}" -eq 1 ] && install=1
        ;;

        "local-zone")
            dir_dest="${dir_dest_unbound}"
            [ "${use_unbound}" -eq 1 ] && install=1
        ;;

    esac

    # if dest folder var exists and install var is true
    if [ -n "${dir_dest}" ] && [ "${install}" -eq 1 ]; then

        # if file exists, mv to file_dated (for backup)
        if [ -f "${dir_dest}/${output}" ]; then

            # if use 'timestamp' time
            if [ "${use_timestamp}" -eq 1 ]; then

                file_bckp="${dir_dest}/${output}.bckp_${timestamp}"

            # if use 'today' time
            else

                file_bckp="${dir_dest}/${output}.bckp_${today}"

            fi

            mv_bckp4bl

        fi

        # cp new file to dest dir service if used services
        if [ -f "${DIR_LISTS}/${output}" ]; then

            if cp "${DIR_LISTS}/${output}" "${dir_dest}/"; then
                [ "${debug}" -eq 1 ] && _log "${txt_file}${DIR_LISTS}/${output}${txt_moved}${dir_dest}"  "OK"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${ttl_installer}" "${ttx_dlg_ok}${txt_file}'${DIR_LISTS}/${output}'${txt_moved}${dir_dest}"
                    sleep 1
                else
                    _mssg "${txt_file}${DIR_LISTS}/${output}${txt_moved}${dir_dest}" "OK"
                fi

                ###
                if [ "${output}" = "hosts" ]; then
                    # add any lines that do not start with #, 127.0.0.1, ::1 of the backuped hosts file into new hosts file
                    if grep -Ev "^\s*(#|127.0.0.1|::1)"  "${file_bckp}" | sed -i "/^::1 localhost/r /dev/stdin" "${dir_dest}/${output}"; then
                        if [ "${dialog}" -eq 1 ]; then
                            _dialog "info" "${ttl_project_name}" "${ttl_installer}" "${ttx_dlg_ok}${txt_lines_added}'${dir_dest}/${output}' "
                            sleep 1
                        else
                            _mssg "${txt_file}${txt_lines_added}'${dir_dest}/${output}' " "OK"
                        fi

                    else
                        if [ "${dialog}" -eq 1 ]; then
                            _dialog "info" "${ttl_project_name}" "${ttl_installer}" "${ttx_dlg_ko}${txt_lines_not_added}'${dir_dest}/${output}' "
                            sleep 1
                        else
                            _mssg "${txt_file}${txt_lines_not_added}'${dir_dest}/${output}' " "KO"
                        fi

                    fi
                fi

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_move_file}${DIR_LISTS}/${output}${txt_into}${dir_dest}/'" "KO"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog -"info" "${ttl_project_name}" "${ttl_installer}" "${txt_dlg_ko}${txt_error_move_file}'${DIR_LISTS}/${output}'${txt_into}${dir_dest}/"
                    sleep 1
                else
                    _mssg "${txt_error_move_file}'${DIR_LISTS}/${output}'${txt_into}${dir_dest}" "KO"
                fi

            fi

        fi

    fi

    unset dir_dest file_bckp install

}

mv_bckp4bl() {

    # attempt to move on backup file
    if mv "${dir_dest}/${output}" "${file_bckp}"; then
        [ "${debug}" -eq 1 ] && _log "${txt_file}'${dir_dest}/${output}'${txt_moved}'${file_bckp}" "OK"

        if [ "${dialog}" -eq 1 ]; then
            _dialog"info" "${ttl_project_name}" "${ttl_installer}" "${ttx_dlg_ok}${txt_file}$'{dir_dest}/${output}'${txt_moved}${file_bckp}"
            sleep 1
        else
            _mssg "${txt_file}'${dir_dest}/${output}'${txt_moved}${file_bckp}" "OK"
        fi

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_move_file}'${dir_dest}/${output}'${txt_into}'${file_bckp}" "KO"

        if [ "${dialog}" -eq 1 ]; then
            _dialog "info" "${ttl_project_name}" "${ttl_installer}" "${txt_dlg_ko}${txt_error_move_file}'${dir_dest}/${output}'${txt_into}${file_bckp}"
            sleep 1
        else
            _mssg "${txt_error_move_file}'${dir_dest}/${output}'${txt_into}${file_bckp}" "KO"
        fi

    fi

}
