########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

_mssg_end() {

    [ "${debug}" -eq 1 ] && _log "### ${txt_goodbye} :: ${txt_ended} ###" "#"

    [ "${cron}" -eq 0 ] && _mssg "
########################################################################
####
##
#   ${txt_goodbye}";

    _mssg "#    ${txt_ended} ";

    [ "${cron}" -eq 0 ] && _mssg "#   ${txt_hope}
##
###
########################################################################
"
    exit 0

}

_welcome() {

    [ "${cron}" -eq 0 ] && _mssg "
########################################################################
####
## ";

    _mssg "#   ${txt_welcome} ";

    [ "${cron}" -eq 0 ] && _mssg "##
###
## ";

    _mssg "#  ${txt_execute}$0${txt_obtain}${list} ";
    [ "${choice}" = "blacklists" ] && _mssg "${txt_choice_bl}${blacklist}";

    [ "${cron}" -eq 0 ] && _mssg "##
###
########################################################################
"

}
