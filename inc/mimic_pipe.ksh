########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

# mimic status of cmd pipe
piper() {
    j=1
    while eval "\${pipestatus_$j+:} false"; do
        unset "pipestatus_$j"
        j=$((j+1))
    done

    j=1 com='' k=1 l=''
    for arg do
        case $arg in
            ('|')
            com="$com {
               $l "'3>&-
               echo "pipestatus_'$j'=$?" >&3
             } 4>&- |'
            j=$((j+1)) l='';;
            (*)
            l="$l \"\${$k}\""
        esac
        k=$((k+1))
    done

    com="$com $l"' 3>&- >&4 4>&-
       echo "pipestatus_'$j'=$?"'

    { eval "$(exec 3>&1; eval "$com")"; } 4>&1
    j=1
    ret=0
    while eval "\${pipestatus_$j+:} false"; do
        eval '[ "$pipestatus_'"$j"'" -eq 0 ] || ret=$pipestatus_'"$j"
        j=$((j+1))
    done
    return "$ret"
}
