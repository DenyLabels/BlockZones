########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabels :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Function: transformer for blacklist into dialog
##
###
########################################################################

transformer_bl() {

    [ "${debug}" -eq 1 ] && _log "# ${ttl_transformer}: ${txt_transform_file}'${FILES[0]}'${txt_in_list}:'${blacklist}'...\n\n ${txt_wait_minutes}"
    _dialog "info" "${ttl_project_name}" "${ttl_transformer}" "${txt_transform_file}'${FILES[0]}':'${blacklist}' \n\n ${txt_wait_minutes}" 10

    case "${blacklist}" in
        "bind"|"bind8"|"bind9")
            format="Bind Config"
            output="bind.zone"
        ;;
        "host"|"hosts"|"host0")
            format="hosts"
            output="hosts"
        ;;
        "unbound")
            format="Local zone"
            output="local-zone"
        ;;
        "unwind")
            format="unwind"
            output="baddomains"
        ;;
    esac

    if [ -f "${FILES[0]}" ];  then

        mssg="###########################
### ${ttl_project_name} ###
###########################
### ${txt_format}'${format}'
### ${txt_data}${now}
##
#"

        _out "${mssg}"
        unset mssg

        case "${blacklist}" in
            "host0")
                _out "0.0.0.0 localhost"
             ;;
             "host"|"hosts")
                if [ "${IPv4}" = 1 ]; then _out "127.0.0.1 localhost"; fi
                if [ "${IPv6}" = 1 ]; then _out "::1 localhost"; fi
             ;;
        esac

        i=0
        while read -r line; do
            # replace '[:space:]' by '\040\011\012\015' for oldier version
            line="$(printf '%s\n' "${line}" | tr -d '[:space:]')"
            #line="$(echo "${line}" | tr -d '[:space:]')"

            case "${blacklist}" in
                "bind8")
                    _out "zone \"${line}\" { type master; notify no; file \"null.zone.file\"; };"
                ;;
                "bind"|"bind9")
                    _out "zone \"${line}\" { type master; notify no; file \"/etc/bind/nullzonefile.txt\"; };"
                ;;
                "host0")
                    _out "0.0.0.0 ${line}"
                ;;
                "host"|"hosts")
                    if [ "${IPv4}" = 1 ]; then _out "127.0.0.1 ${line}"; fi
                    if [ "${IPv6}" = 1 ]; then _out "::1 ${line}"; fi
                ;;
                "unwind")
                    _out "${line}"
                ;;
                "unbound")
                    if [ "${IPv4}" = 1 ]; then
                        if [ "${USE_LZ_REDIRECT}" = 1 ]; then
                            _out "local-zone: \"${line}\" redirect\nlocal-data: \"${line} A 127.0.0.1\""
                            #echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}";
                        else
                            _out "local-data: \"${line} A 127.0.0.1\""
                            #echo -e "local-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}";
                        fi
                    fi
                    if [ "${IPv6}" = 1 ]; then
                        if [ "${USE_LZ_REDIRECT}" = 1 ]; then
                            _out "local-zone: \"${line}\" redirect\nlocal-data: \"${line} AAAA ::1\""
                            #echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}";
                        else
                            _out "local-data: \"${line} AAAA ::1\""
                            #echo -e "local-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}";
                        fi
                    fi
                ;;
            esac

            #let i++
            (( i++ ))
        done  < "${FILES[0]}"
        unset i

        del_file "${FILES[0]}"

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_no_file}${FILES[0]}" "KO"
        _dialog "mssg" "${ttl_project_name}" "${ttl_transformer}" "${txt_dlg_ko}${txt_error_no_file}'${FILES[0]}'"
        sleep 1

        byebye

    fi

    unset arg

    if [ -f "${DIR_LISTS}/${output}" ]; then
        [ "${debug}" -eq 1 ] && _log "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" "OK"
        _dialog "info" "${ttl_project_name}" "${ttl_transformer}" "${txt_dlg_ok}${txt_file}'${DIR_LISTS}/${output}'${txt_builded}"
        sleep 1

        build_sums

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_create_checksum_file}${DIR_LISTS}/${output}" "KO"
        _dialog "mssg" "${ttl_project_name}" "${ttl_transformer}" "${txt_dlg_ko}${txt_error_create_checksum_file}${DIR_LISTS}/${output}"
        sleep 1

        byebye

    fi

}
