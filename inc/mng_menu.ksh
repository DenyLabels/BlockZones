########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

# build menus
get_array_menu() {

    case "${choice}" in

        "blacklists")
            set -A NAMES -- "${menus_blacklists[@]}"
        ;;

        *)
            set -A NAMES -- "${menus[@]}"
        ;;

    esac

    typeset -i nb="${#NAMES[@]}" i=0 j=0

    while (( i < nb )); do

        n="${NAMES[$i]}"

        if [ "${choice}" = "blacklists" ]; then
            text="txt_menus_bl_$n"
        else
            text="txt_menus_$n"
        fi

        [[ "$VKSH" == *"PD\ KSH"* ]] && eval "ref=\${$text}" || typeset -n ref="${text}"
        #eval "ref=\${$text}"

        dialog_menu[$j]="${NAMES[$i]}"
        dialog_menu[$j+1]="${ref}"

        (( j+=2 ))
        (( i+=1 ))

        unset ref text n

    done

}

menu() {

    PS3="${txt_menu_ps3}"
    select option in "${menus[@]}"; do
        choice="$option"; break
    done

    _log "
${txt_choice}${choice}
    "

    case "${choice}" in

        "blacklists")

            PS3="${txt_menu_ps3_bl}"
            select option in "${menus_blacklists[@]}"; do
                blacklist="${option}"; break
            done

            _log "
${txt_choice_bl}${blacklist}
            "
        ;;

        "quit") _mssg_end ;;

    esac

    sleep 1

    setVariables

}
