########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### Get data into file in array...
build_lists() {

    [ "${debug}" -eq 1 ] && _log "${ttl_mng_list}" "#"
    [ "${verbose}" -eq 1 ] && _mssg "*${ttl_mng_list}" "#"

    file="${DIR_SRC}/${list}"

    if [ -f "${file}" ]; then

        [ "${debug}" -eq 1 ] && _log "*** ${txt_mng_list} ${file} "
        [ "${verbose}" -eq 1 ] && _mssg "*** ${txt_mng_list} ${file} ***" "#"

        sed -e "/^#/d;/^$/d;s/ \+//g" "${file}" > "${file}.${choice}"

        if [ -f "${file}.${choice}" ]; then
            i=0
            while read -r line; do

                lists[$i]="${line}"
                (( i++ ))

            done < "${file}.${choice}"
            unset i

            [ "${debug}" -eq 1 ] && printf '%s \n%s\n'  "*** lists:" "${lists[@]}"

            del_file "${file}.${choice}" "p"

        fi

    else
        [ "${debug}" -eq 1 ] && _log "${txt_file}${file}${txt_not_readable}" "KO"
        _mssg "${txt_file}${file}${txt_not_readable}" "KO"

        byebye

    fi

    unset file

}

build_uniq_list() {

    # change to lower case, and sort
    file="${FILES[0]}"

    if [ -f "${file}" ]; then
        tr -ds '[:upper:]' '[:lower:]' < "${file}" > "${file}.uniq"
        sort -u -o "${file}" "${file}.uniq"
        del_file "${file}.uniq" "p"
    fi

    unset file
}

is_install_list() {

    if [ "${install_list}" -eq 1 ]; then detect_uid; fi

}

make_uniq_list() {

    touch_files

    [ "${debug}" -eq 1 ] && _log "${txt_make_single_file}'${filename}' " "#"
    [ "${verbose}" -eq 1 ] && _mssg "${txt_make_single_file}'${filename}' " "hb"

    case "${list}" in
        "badips")

            [ "${debug}" -eq 1 ] && _log "* ${txt_check_ipv4}: ${filename} > ${FILES[0]}"
            [ "${verbose}" -eq 1 ] && _mssg "${txt_check_ipv4}" "hb"
            valid_ipv4 "${filename}" >> "${FILES[0]}"

            [ "${debug}" -eq 1 ] && _log "* ${txt_check_ipv6}: ${filename} > ${FILES[1]}"
            [ "${verbose}" -eq 1 ] && _mssg "${txt_check_ipv6}" "hb"
            valid_ipv6 "${filename}" >> "${FILES[1]}"

        ;;
        "bogons")

            case "${url}" in
                *"bogons_ipv4"*)

                    [ "${debug}" -eq 1 ] && _log "* ${txt_check_ipv4}: ${filename} > ${FILES[0]}"
                    [ "${verbose}" -eq 1 ] && _mssg "${txt_check_ipv4}" "hb"
                    valid_ipv4 "${filename}" >> "${FILES[0]}"

                ;;
                *"bogons_ipv6"*)

                    [ "${debug}" -eq 1 ] && _log "* ${txt_check_ipv6}: ${filename} > ${FILES[1]}"
                    [ "${verbose}" -eq 1 ] && _mssg "${txt_check_ipv6}" "hb"
                    valid_ipv6 "${filename}" >> "${FILES[1]}"

                ;;

            esac

        ;;
        "domains")

            [ "${debug}" -eq 1 ] && _log "${txt_lower_domains}${filename} > ${FILES[0]}" "#"
            awk '{ print tolower($0) }' "${filename}" >> "${FILES[0]}"

        ;;
    esac

    del_spaces

}

mng_lists() {

    [ "${debug}" -eq 1 ] && _log "${ttl_manager}" "#"
    [ "${verbose}" -eq 1 ] && _mssg "${ttl_manager}" "#"

    if [ "${dialog}" -eq 1 ]; then
        . "${DIR_INC}/mngr_lists.dialog.ksh"
    else
        . "${DIR_INC}/mngr_lists.ksh"

    fi

    mngr_lists

}
