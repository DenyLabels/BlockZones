########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/01/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Function: manager for lists into dialog
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mngr_lists() {

    [ "${debug}" -eq 1 ] && _log "# ${ttl_manager}: ${txt_read_array_bl}"
    _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_read_array_bl}" 10

    count="${#lists[@]}"
    modulo=$(printf '%s\n' "100/$count"|bc)

    [ "${debug}" -eq 1 ] && _log "# manager: count: $count | modulo: $modulo"

    if [ "${count}" -gt 0 ]; then

        (

        for url in "${lists[@]}"; do

            file="$(printf '%s\n' "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";

            if [ "${choice}" != "bogons" ]; then
                ndd="$(printf '%s\n' "${url}" | awk -F'/' '{ print $3 }')"
                
                if [[ "${url}" == *"StevenBlack"* ]] && [ "${ndd}" = "raw.githubusercontent.com" ]; then
                    name="$(printf '%s\n' "${url}" | awk -F'/' '{ print "-"$4"-"$8"-"$9"-" }')"
                    name="${ndd}${name}${file}"
                    is_StevenBlack=1
                else
                    name="${ndd}_${file}"
                fi

                if [ "${choice}" = "blacklists" ] && [ "${url}" = "personals" ]; then
                    filename="${DIR_SRC}/${url}"
                else
                    filename="${DIR_DL}/${name}"
                fi

            fi

            if [ "${debug}" -eq 1 ]; then
                [ -n "${ndd}" ] && _log "# ${txt_domain}${ndd}"
                [ -n "${file}" ] && _log "# ${txt_file}${file}"
                [ -n "${filename}" ] && _log "# ${txt_filename}${filename}"
            fi

            let x+=$modulo

            cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $name
XXX
EOF

            if [ "${choice}" != "bogons" ]; then
                # define seconds before new dl
                case "${ndd}" in
                    "feeds.dshield.org") seconds=259200 ;;  # 3 days
                    "lists.blocklist.de") seconds=172800 ;; # 2 days
                    "mirror1.malwaredomains.com") seconds=2592000;;   # 1 month
                    "myip.ms") seconds=864000 ;;    # 10 days                    
                    "osint.bambenekconsulting.com") seconds=600 ;; # 10 min.
                    "ransomwaretracker.abuse.ch") seconds=2592000 ;;    # 30 days
                    "raw.githubusercontent.com") 
                        if [ "${is_StevenBlack}" -eq 1 ]; then seconds=604800; fi # 7 days
                    ;;
                    #"sslbl.abuse.ch") seconds=900 ;; # 15 min.
                    "www.openbl.org") seconds=172800 ;;    # 2 days
                    "winhelp2002.mvps.org") seconds=604800;; # 7 days
                    "www.spamhaus.org") seconds=3600;; # 1 hours
                    *) seconds=86400;;
                esac
            fi

            if [ "${debug}" -eq 1 ] ; then
                if [ "${url}" != "personals" ] || \
                    [[ "${url}" != *"bz_bogons"* ]]; then
                    _log "# ${txt_nb_seconds}${seconds}"
                fi
            fi

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(printf '%s\n' "${timestamp} - ${file_seconds}" | bc)

                unset file_seconds

                if [ "${diff_sec}" -gt "${seconds}" ]; then

                    if [ "${url}" != "personals" ] || \
                        [[ "${url}" != *"bz_bogons"* ]]; then

                        del_file "${filename}"
                        sleep 1
                        download

                    fi

                fi

            else

                if [ "${url}" != "personals" ] || \
                    [[ "${url}" != *"bz_bogons"* ]]; then download; fi

            fi

            if [ -f "${filename}" ]; then

                if [ "${choice}" != "bogons" ]; then
                    get_mimes
                    uncompress

                    case "${ndd}" in
                        "hosts-file.net")
                            if [ "${mimetype}" = "application/zip" ]; then
                                filename="${filename%.zip}/hosts.txt"
                            fi
                        ;;
                        "mirror1.malwaredomains.com")
                            if [ "${mimetype}" = "application/zip" ]; then
                                case "${file}" in
                                    "immortal_domains.zip")
                                        filename="${filename%.zip}/${file%.zip}.txt"
                                    ;;
                                    "justdomains.zip"|"malwaredomains.zones.zip")
                                        filename="${filename%.zip}/${file%.zip}"
                                    ;;
                                esac
                            fi
                        ;;
                        "winhelp2002.mvps.org")
                            if [ "${mimetype}" = "application/zip" ]; then
                                filename="${filename%.zip}/HOSTS"
                            fi
                        ;;
                        "www.openbl.org")
                            #if [ "${mimetype}" = "application/gzip" ] || [ "${mimetype}" = "application/x-gzip" ]; then
                            if [[ "${mimetype}" == *"gzip"* ]]; then
                                filename="${filename%.gz}"
                            fi
                        ;;
                    esac

                fi

                purge_files

                make_uniq_list

                unset filename name ndd file

            fi

        done

        ) | _dialog "info" "${ttl_project_name}" "${ttl_manager}" "${txt_wait}" 10

    else
        [ "${debug}" -eq 1 ] && _log "${txt_error_no_data}" "KO"
        _dialog "mssg" "${ttl_project_name}" "${ttl_manager}" "${txt_dlg_ko}${txt_error_no_data}"
        sleep 1

        byebye

    fi

    sleep 1

    unset count

}
