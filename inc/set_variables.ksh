########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

setVariables() {

    [ "${debug}" -eq 1 ] && _log "menu: $menu"

    list="${menu}"
    [ -z "$choice" ] && choice="${menu}"

    case "${menu}" in

        "badips")     setVarsBadips ;;

        "blacklists") setVarsBlacklists ;;

        "bogons")     setVarsBogons ;;

        "menu")

            list="${choice}"

            case "${choice}" in

                "badips")   setVarsBadips ;;
                "blacklists")   setVarsBlacklists ;;
                "bogons")   setVarsBogons ;;

            esac

        ;;

    esac

    if [ "${debug}" -eq 1 ]; then
        if [ "${menu}" = "blacklists" ]; then
            _log "## setVariables: list: $list | choice: $choice | blacklist: $blacklist"
        else
            _log "## setVariables: list: $list | choice: $choice"
        fi
        _log "## ${txt_files}${FILES[@]}"
        _log "## IPv4: ${IPv4} | IPv6: ${IPv6} "
    fi

    if [ -z "${choice}" ]; then
        _mssg "${txt_error_no_var}'\$choice'" "KO"
        _log "${txt_error_no_var}'\$choice'" "KO"
        byebye;
    fi

    if [ -z "${list}" ]; then
        _mssg "${txt_error_no_var}'\$list'" "KO"
        _log "${txt_error_no_var}'\$list'" "KO"
        byebye;
    fi

}

setVarsBadips() {

    FILES[0]="${DIR_SRC}/${list}_ipv4"
    FILES[1]="${DIR_SRC}/${list}_ipv6"

}

setVarsBlacklists() {

    list="domains"

    FILES[0]="${DIR_SRC}/${list}_${choice}"

    if [ "${choice}" = "blacklists" ]; then
        ARG="${blacklist-ARG}"
        [ "$ARG" = "ARG" ] && blacklist="unbound"
        [[ ${menus_blacklists[@]} != *"$ARG"* ]] && blacklist="unbound" || blacklist="${ARG}"
    fi

}

setVarsBogons() {

    FILES[0]="${DIR_SRC}/${list}_ipv4"
    FILES[1]="${DIR_SRC}/${list}_ipv6"

}
