########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### Create sha512 checksums files!
build_sums() {

    if [ "${one_checksum_file}" = 0 ]; then

        [ "${debug}" -eq 1 ] && _log "${ttl_buildsum}" "#"
        [ "${verbose}" -eq 1 ] && _mssg "*${ttl_buildsum}" "#"

        typeset bool=1

        case "${list}" in
            "bogons") output="${file}" ;;
        esac

        if [ -f "${DIR_LISTS}/${output}" ];  then

            cd "${DIR_LISTS}" || exit 1

            case "${OSN}" in

                "FreeBSD")
                    if sha512 "${output}" > "${output}.sha512"; then bool=0; fi
                ;;

                "OpenBSD")
                    if sha512 -h "${output}.sha512" "${output}"; then bool=0; fi
                ;;

            esac

            cd "${ROOT}" || exit 1

            if [ "${bool}" -eq 0 ]; then
                [ "${debug}" -eq 1 ] && _log "${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}" "OK"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${title_checksums}" "${txt_dlg_ok}${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}"
                    sleep 1
                else
                    _mssg "${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}" "OK"
                fi
                return 0

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_create_checksum_file}'${DIR_LISTS}/${output}' " "KO"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${title_checksums}" "${txt_dlg_ko}${txt_error_create_checksum_file}'${DIR_LISTS}/${output}' "
                    sleep 1
                else
                    _mssg "${txt_error_create_checksum_file}${DIR_LISTS}/${output}" "KO"
                fi
                return 1

            fi

        fi

        unset bool

    fi

}

create_one_sums() {

    if [ "${one_checksum_file}" = 1 ]; then

        [ "${debug}" -eq 1 ] && _log "${ttl_buildsum}" "#"
        [ "${verbose}" -eq 1 ] && _mssg "*${ttl_buildsum}" "#"

        cd "${DIR_LISTS}" || exit 1

        file="${DIR_SRC}/${NAME}.sha512"

        find ./ -type f -exec sha512 {} + > "${file}";

        if [ -f "${file}" ]; then

            [ "${debug}" -eq 1 ] && _log "${txt_file_checksum}'${file}'${txt_created}" "OK"
            if [ "${dialog}" -eq 1 ]; then
                _dialog "info" "${ttl_project_name}" "${ttl_create_checksum}" "${ttx_dlg_ok}${txt_file_checksum}'${file}'${txt_created}"
                sleep 1

            else
                _mssg "${txt_file_checksum}'${file}'${txt_created}" "OK"

            fi

            if mv "${file}" "${DIR_LISTS}"; then

                [ "${debug}" -eq 1 ] && _log "${txt_file_checksum}'${file}'${txt_moved}'${DIR_LISTS}' " "OK"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${ttl_create_checksum}s" "${ttx_dlg_ok}${txt_file_checksum}'${file}'${txt_moved}'${DIR_LISTS}' "
                    sleep 1

                else
                    _mssg "${txt_file_checksum}'${file}'${txt_moved}'${DIR_LISTS}' " "OK"

                fi

                create_sign

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_move_file}'${file}'${txt_into}'${DIR_LISTS}' " "KO"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${ttl_create_checksum}" "${txt_dlg_ko}${txt_error_move_file}'${file}'${txt_into}'${DIR_LISTS}' "
                    sleep 1

                else
                    _mssg "${txt_error_move_file}'${file}'${txt_into}'${DIR_LISTS}' " "KO"

                fi

            fi

        else
            [ "${debug}" -eq 1 ] && _log "${txt_error_create_checksum_file}'${file}'" "KO"
            if [ "${dialog}" -eq 1 ]; then
                _dialog "info" "${ttl_project_name}" "${ttl_create_checksum}" "${txt_dlg_ko}${txt_error_create_checksum_file}'${file}' "
                sleep 1

            else
                _mssg "${txt_error_create_checksum_file}'${file}' " "KO"

            fi

        fi

        unset file

        cd "${ROOT}" || exit 1

    fi

}

create_sign() {

    if [ "${use_sign}" = 1 ]; then

        if [ "${one_checksum_file}" = 1 ]; then

            file_sha="${DIR_LISTS}/${NAME}.sha512"
            file_sig="${file_sha}.sig"

            if signify -S -s "${dir_sec_key_signify}" -m "${file_sha}" -e -x "${file_sig}"; then

                [ "${debug}" -eq 1 ] && _log "${txt_file_sign}'${file_sig}'${txt_created}" "OK"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${ttl_create_sign}" "${txt_dlg_ok}${txt_file_sign}'${file_sig}'${txt_created}"
                    sleep 1

                else
                    _mssg "${txt_file_sign}'${file_sig}'${txt_created}" "OK"

                fi

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_create_sign_file}'${file_sig}" "KO"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${ttl_create_sign}" "${txt_dlg_ko}${txt_error_create_sign_file}'${file_sig}' "
                    sleep 1
                else
                    _mssg "${txt_error_create_sign_file}'${file_sig}' " "KO"
                fi

            fi

        else
            file_sha="${DIR_LISTS}/${output}.sha512"
            file_sig="${file_sha}.sig"

            if signify -S -s "${dir_sec_key_signify}" -m "${file_sha}" -e -x "${file_sig}"; then

                [ "${debug}" -eq 1 ] && _log "${txt_file_sign}'${file_sig}'${txt_created}'" "OK"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog"info" "${ttl_project_name}" "${ttl_create_sign}" "${txt_dlg_ok}${txt_file_sign}'${file_sig}'${txt_created}"
                    sleep 1
                else
                    _mssg "${txt_file_sign}'${file_sig}'${txt_created}" "OK"
                fi

            else
                [ "${debug}" -eq 1 ] && _log "${txt_error_create_sign_file}'${file_sig}'" "KO"
                if [ "${dialog}" -eq 1 ]; then
                    _dialog "info" "${ttl_project_name}" "${ttl_create_sign}" "${txt_dlg_ko}${txt_error_create_sign_file}'${file_sig}' "
                    sleep 1
                else
                    _mssg "${txt_error_create_sign_file}'${file_sig}' " "KO"
                fi

            fi

        fi

        unset file_sha file_sig

    fi

}

del_sums() {

    if [ "${one_checksum_file}" = 1 ]; then
        find "${DIR_LISTS}/" -type f -name "*.sha512*" -delete

    else
        del_file "${DIR_LISTS}/${NAME}.sha512*" "p"

    fi

}
