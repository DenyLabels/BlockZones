########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

purge_files() {

    [ "${debug}" -eq 1 ] && _log "${ttl_purge_files}" "#"
    [ "${verbose}" -eq 1 ] && _mssg "${ttl_purge_files}" "#"

    case "${choice}" in

        "badips")       purge_files_bi ;;
        "blacklists")   purge_files_bl ;;
        "bogons")       purge_files_bg ;;

    esac

}

purge_files_bg() {

    [[ "${url}" != *"bz_bogons"* ]] && sed -i -e "${p_blank};" "${filename}"

    case "${filename}" in
        *"fullbogons-ipv4"*)
            sed -i -e "s/192\.168\(.*\)/#192.168\1/" "${filename}"
        ;;
        #"fullbogons-ipv6.txt")
            #sed -i -e "" "${filename}"
        #;;
    esac

}

purge_files_bi() {

    [ "${debug}" -eq 1 ] && _log "* ${txt_purge_files}${filename}"

    case "${ndd}" in
        "danger.rulez.sk")  # delete line ^#, and keep only adr ip
            #sed -i -e "/^#/d;s/^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*/\1/g;s/ \+//g" "${filename}"
            sed -i -e "${p_blank};${p_before}" "${filename}"
        ;;
        "feeds.dshield.org")    # delete empty lines, and ^#
            #sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "s#^Start\(.*\)#\#Start\1#g;${p_blank}" "${filename}"

            del_file "${filename}_awk" "p"

            while read -r line; do
                printf '%s\n' "${line}" | awk '{print $1"/"$3}' >> "${filename}_awk"
                #echo "${line}" | awk '{print $1"/"$3}' >> "${filename}_awk"
            done < "${filename}"

            filename="${filename}_awk"
            #sed -i -e "s#\(.*\)\/$#\1#g" "${filename}"

        ;;
        "isc.sans.edu")
            # see: https://www.geoghegan.ca/pfbadhost.html
            grep -Eos '([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}' "${filename}" > "${filename}_grep"
            filename="${filename}_grep"
        ;;
        "malc0de.com")  # delete empty lines, and ^//
            sed -i -e "${p_blank};/^\/\//d;${p_space}" "${filename}"
        ;;
        "myip.ms")  # delete empty lines, spaces, and keep only adr ip
            sed -i -e "${p_blank};${p_before};${p_space}" "${filename}"
        ;;
        "osint.bambenekconsulting.com")
            sed -i -e "${p_blank};s#\(\([^,]\+,\)\{1\}\)[^,]\+,\(.*\)#\1#g;${p_space}" "${filename}"
        ;;
        "sslbl.abuse.ch")   # delete empty lines, ^#, ...
            #sed -i -e "/^$/d;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\,.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "${p_blank};s#\(.*\),\(.*\),\(.*\)#\1#g;${p_space}" "${filename}"
        ;;
        "www.spamhaus.org") # delete empty lines, ...
            sed -i -e "${p_blank};${p_sbl}" "${filename}"
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "${p_blank}" "${filename}"
        ;;
    esac

}

purge_files_bl() {

    case "${ndd}" in
        "adaway.org")
            #case "${ARG}" in
                #"host"|"hosts"|"host0")
                    #sed -i -e "${p_blank};s/\(.*\) ##\(.*\)#/\1/g;${p_local}" "${filename}"
                #;;
                #*)
                    #sed -i -e "${p_blank};s/\(.*\) ##\(.*\)#/\1/g;${p_local}" "${filename}"
                #;;
            #esac
            sed -i -e "${p_blank};s/\(.*\) ##\(.*\)#/\1/g;${p_local}" "${filename}"
        ;;
        "hosts-file.net")
            #case "${ARG}" in
                #"host"|"hosts"|"host0")
                    #sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s/ \+//g" "${filename}"
                #;;
                #*)
                    #sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1\t\(.*\)#\1#g;s/ \+//g" "${filename}"
                #;;
            #esac
            sed -i -e "${p_blank};${p_local}" "${filename}"
        ;;
        "mirror1.malwaredomains.com")
            case "${file}" in
                "immortal_domains.zip")
                    sed -i -e "${p_blank};/^notice/d;${p_space}" "${filename}"
                ;;
                "malwaredomains.zones.zip")
                    #sed -i -e "/^\/\//d;s/ \+/ /g;s#zone \"\(.*\)\" {type master; file \"/etc/namedb/blockeddomain.hosts\";};#\1#g" "${filename}"
                    { del_file "${filename}" && sed -e "${p_blank};/^\/\//d;" | awk '{print $2}' | sed -e "s/\"//g" | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        # for Steven Black lists:
        "raw.githubusercontent.com")
            if [ "${is_StevenBlack}" -eq 1 ]; then 
                sed -i -e "${p_blank};${p_bdcst};${p_local};${p_ip6};${p_zero};${p_before}" "${filename}"
            fi
        ;;
        "someonewhocares.org")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "${p_blank};${p_local};${p_bdcst};${p_before};/^$/d" "${filename}"
                ;;
                *)
                    #sed -i -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;/^$/d;s#127.0.0.1 \(.*\)#\1#g" "${filename}"
                    { del_file "${filename}" && sed -e "${p_blank};${p_local};${p_bdcst}" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        "winhelp2002.mvps.org")
            case "${ARG}" in
                "host0")
                    sed -i -e "${p_blank};/^\r/d;${p_local};${p_before};${p_space}" "${filename}"
                ;;
                *)
                    sed -i -e "${p_blank};/^\r/d;${p_local};${p_zero};${p_before};${p_space}" "${filename}"
                ;;
            esac
        ;;
        "www.malwaredomainlist.com")
            #case "${ARG}" in
                #"host"|"hosts"|"host0")
                    ##sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" "${filename}"
                    #{ del_file "${filename}" && sed -e "${p_blank};${p_local}" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                #;;
                #*)
                    ##sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1  \(.*\)#\1#g" "${filename}" #;
                    #{ del_file "${filename}" && sed -e "${p_blank};${p_local}" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                #;;
            #esac
            { del_file "${filename}" && sed -e "${p_blank};${p_local}" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "${p_blank}" "${filename}"
        ;;
    esac

}
