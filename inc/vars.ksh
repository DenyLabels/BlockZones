########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/05
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### DO NOT TOUCH!
OLD_TERM="$(echo $TERM)"
export TERM=xterm

### Project Name; DO NOT TOUCH!
NAME="BlockZones"

### Detect Ksh Version; DO NOT TOUCH!
VKSH="$(echo $KSH_VERSION)"

### Declare directories; DO NOT TOUCH!
DIR_DL="${ROOT}/downloads"
DIR_INC="${ROOT}/inc"
DIR_LANG="${ROOT}/lang"
DIR_LISTS="${ROOT}/lists"
DIR_LOG="${ROOT}/log"
DIR_SRC="${ROOT}/src"

########################################################################
### load project configurables variable file
. "${DIR_INC}/cfg_vars.ksh"
### superseed with personals configurables variables file
if [ -f "${DIR_INC}/cfg_vars.local" ]; then
    . "${DIR_INC}/cfg_vars.local"
fi
########################################################################

# disable few variables segun conditions...
if [ $cron -eq 1 ]; then
    dialog=0
    use_color=0
    verbose=0
fi

### destination directories services
dir_dest_bind="/etc/bind"
dir_dest_bz="/etc/${NAME}"
dir_dest_host="/etc"
dir_dest_unbound="/var/unbound/etc"

### Signify files
dir_pub_key_signify="${DIR_INC}/BlockZones_GenLists.pub"
dir_sec_key_signify="${DIR_INC}/BlockZones_GenLists.sec"

### to detect tools; DO NOT TOUCH!
dldr='ftp'
typeset -i use_bind=0
if [ -z "${use_curl}" ]; then typeset -i use_curl=0; fi
if [ -z "${use_hosts}" ]; then typeset -i use_hosts=0; fi
typeset -i use_unbound=0
typeset -i use_unwind=0
if [ -z "${use_wget}" ]; then typeset -i use_wget=0; fi

### declare OS variables; DO NOT TOUCH!
OSN="$(uname -s)"   # Get Operating System Name
OSR="$(uname -r)"   # Get Operating System Release
# Declare user agent for curl, wget,...
RV="67.0"   # revision number
UA="Mozilla/5.0 (X11; ${OSN}; rv:${RV}) Gecko/20100101 Firefox/${RV}"

### declare color variables; DO NOT TOUCH!
if [ $use_color -eq 1 ]; then
    bold="$(tput bold)"
    dim="$(tput dim)"
    green="$(tput setaf 2)"
    neutral="$(tput sgr0)"
    red="$(tput setaf 1)"
fi

### others variables; DO NOT TOUCH
blacklist=""    # var blacklist when menu = blacklists
bz_log=""   # filenamge log; use by debug option
choice=""   # variable choice to value menu
list=""     # list name
mimetype=""
output=""   # filename for blacklists output; DO NOT TOUCH!
is_StevenBlack=0

### dates; DO NOT TOUCH
now="$(date +"%x %X")"
today="$(date +'%Y-%m-%d_%H-%M-%S')"
timestamp="$(date +%s)"

typeset -i seconds=86400   #  default delay, in seconds, before downloads list, again. Modify only with precautions!

### lang; DO NOT TOUCH!
lang="$(echo ${LANG})"
[ -z "${lang}" ] && lang="$(echo ${LC_MESSAGES})"
[ -z "${lang}" ] && lang="en"
lang="$(echo "${lang}" | awk '{ print substr($0,0,2) }')"
#[[ "$VKSH" == *"PD\ KSH"* ]] && lang="$(printf '%.2s\n' "${lang}")" || lang="${lang:0:2}"   # substring two first caracters

### patterns for sed; DO NOT TOUCH!
# /^$/d     <= empty line
# /^#/d     <= line starting with #
# /^\s*$/d  <= remove spaces'n tabs = '/^[[:space:]]*$/d'
# s/ \+//g  <= remove all spaces
# s/ \+/ /g  <= replace more spaces by only one
### replace '[:space:]' by '\040\011\012\015' for oldier version
p_blank="/^#/d;/^$/d;/^[[:blank:]]*#/d"
p_bdcst="/\(.*\)broadcast\(.*\)/d"
p_before="s/^\(.*\)#\(.*\)/\1/g;s/^\(.*\)  #\(.*\)/\1/g"
p_ip6="/^fe80::1/d;/^ff0[02]::/d;/^::1/d"
p_local="/\(.*\)localhost\(.*\)/d;/\(.*\)local\(.*\)/d;s#127.0.0.1[[:space:]]*\(.*\)#\1#g"
p_sbl="s#\(.*\) \; SBL\(.*\)#\1#g"
p_space="/^[[:space:]]*$/d"
p_zero="/^0.0.0.0 0.0.0.0$/d;s#0.0.0.0 \(.*\)#\1#g"

# few arrays; DO NOT TOUCH!
set -A lines
set -A lists
set -A FILES
set -A menus 'badips' 'blacklists' 'bogons' 'quit'
set -A menus_blacklists 'unbound' 'bind8' 'bind9' 'hosts' 'host0' 'unwind'

########################################################################
