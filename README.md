BlockZones Project
==================

English
-------

**2020: I stop this project!**

Project to blacklist bad domains names, and bad adresses IP, *knew for bad activities*:
- ADS servers
- malwares, trackers, and others badlies
- bogons
- bad IPs

=> Please, see files, lists:
- [badips](badips)
- [bad domains](domains)

or, better, see this excellent project, for:
- unbound: **[unbound-badhost][1]** 
- or, PF: **[pf-badhost][2]**!

Français
--------

**2020: J'arrête ce projet.**

Projet pour blacklister des noms de domaines, et des adresses ip, *connus pour leur activité suspecte*, relatifs :
- serveurs ADS - publicitaires
- malwares, trackers, et autres "méchancetés".
- réseaux "bogons"
- et autres "badips" ...

=> Veuillez lire les fichiers suivants, qui contiennent les listes pour :
- [badips](badips)
- [mauvais domaines](domains)

ou, mieux, allez voir cet excellent projet nommé :
- pour unbound : **[unbound-badhost][1]** 
- ou, pour PF : **[pf-badhost][2]** !

---

[1]: https://www.geoghegan.ca/unbound-adblock.html
[2]: https://www.geoghegan.ca/pfbadhost.html
