#!/bin/ksh
#set -x
[ -n "$TERM" ] && clear
########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/BlockZones.git
#
#   This file is part of "DenyLabel :: BlockZones Project"
#
# Date: 2019/02/04
#
##
###
########################################################################
###
##
#   BlockZones single script, with menu
##
###
########################################################################
###
##
#   For OpenBSD: ksh - public domain Korn Shell
##
###
########################################################################

ROOT="$(dirname "$(readlink -f -- "$0")")"

. "${ROOT}/inc/vars.ksh"

########################################################################
###
##
#    EXECUTION: DO NOT TOUCH!
##
###
########################################################################

. "${DIR_INC}/bz_execution.sh"
