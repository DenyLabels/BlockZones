# BlockZones

Projet pour blacklister des noms de domaines, et des adresses IP, *connus pour leur activité suspecte*, relatifs :
- serveurs ADS - publicitaires
- malwares, trackers, et autres "méchancetés".
- réseaux "bogons"
- et autres "badips"...

**Retrouvez ces listes mises-à-jours tous les jours, avec leurs fichiers de sommes de contrôle sha512, à l'adresse suivante :** <br/>
[https://stephane-huc.net/share/BlockZones/lists/][1]

/!\ **ATTENTION : profond remaniement, un seul script pour tout : bz.ksh ou bz.ksh93 !!!** /!\

----------

## Utilisation

```shell
Usage: ./bz.ksh options
    Options:
    -b: pour générer les listes 'bogons', une pour IPv4, l'autre pour IPv6 !
    -h: pour afficher le menu d'aide.
    -i: pour générer les listes 'badips', une pour IPv4, l'autre pour IPv6 !
    -l: pour générer les listes 'blackslists' ; les arguments possibles sont 'unbound', 'bind8', 'bind9', 'hosts', 'host0', 'unwind'...
    -m: pour utiliser ce script avec un menu...
```

Il n'existe qu'en version pdksh, pour OpenBSD, et version ksh93, pour \*BSD.

----------

### Option `badips`

=> L'option `badips` agit en plusieurs temps :

- elle télécharge les listes de fichiers enregistrés dans le fichier `src/badips`, en tenant compte d'un certain délai de latence afin de ne pas trop revisiter les sites enregistrés
- elle les traitent pour créer deux listes uniques, une `lists/badips\_ipv4` et une autre `lists/badips\_ipv6`... respectivement pour IPv4 et IPv6, qui sont recréées à chaque lancement du script, avec leur propre fichier de somme de contrôle sha512.

=> **Si dans le script de configuration `inc/cfg_vars.*`**, l'option `install_list` est égale à `1` :

- **le script `bz.ksh*` doit être exécuté avec des droits administrateurs** - sinon il s’arrêtera - :
  - afin de pouvoir copier les listes générées dans le répertoire `/etc/BlockZones`.
  - et de redémarrer la lecture des tables PF. Dans ce cas, veillez IMPÉRATIVEMENT à créer/configurer des tables pour PF, telles que les règles ci-dessous, et à remplir les noms exacts dans le tableau `pf_tables` de votre script de configuration. **Une erreur dans un des noms des tables PF générera une erreur d'installation**.

=> règles PF IPv4 :
```
table <t_badips> persist file "/dir/badips_ipv4"

block drop in quick on egress from { <t_badips> } to any
block drop out quick on egress from any to { <t_badips> }
```

=> règles PF IPv6 :
```
table <t_badips6> persist file "/dir/badips_ipv6"

block drop in quick on egress inet6 from { <t_badips6> } to any
block drop out quick on egress inet6 from any to { <t_badips6> }
```

**Attention** : Il semble nécessaire d'agrandir le nombre des entrées de tables... si vous voulez utiliser toutes les adresses retournées.

----------

### Option `blacklists`

=> L'option `blacklists` agit en plusieurs temps :

- elle télécharge les listes de fichiers autorisés dans le fichier `src/domains`, en tenant compte d'un délai de latence pour certains qui n'acceptent pas qu'on les revisitent trop souvent, au cas où le script serait relancé plusieurs fois dans la journée. Il suffit de les décommenter pour que le script `blacklists` les gèrent.
- elle les traitent pour créer une liste unique `lists/uniq_domains`... qui est récrée à chaque lancement du script.
- puis, il créé un fichier :
  - `lists/local-zone` pour traitement par unbound,
  - `lists/bind.zone` pour traitement par bind (version 8, et 9),
  - ou, `lists/hosts` pour un traitement local par fichier hosts ...
  - voire `lists/baddomains` pour un traitement par unwind - *depuis OpenBSD 6.5*. 
- ainsi que leur propre fichier de somme de contrôle sha512 !

Les arguments sont :

- `unbound`, pour le service 'unbound'
- `bind8`, `bind9`, pour le service 'Bind'
- `hosts`, pour le fichier `/etc/hosts` - ou `host0` également pour le fichier `/etc/hosts`, mais avec les adresses commençant en `0.0.0.0` et non pas en `127.0.0.1`
- `unwind`, pour le service 'unwind'

**ATTENTION** : Si vous activez toutes les URL référencées dans le fichier `lists/domains`, il est possible que le service devant traiter la liste unique finale ne puisse le faire par manque de ressources mémoires.
De même, cela augmentera le temps de traitement et de création de la liste unique par votre machine.

=> **Si dans le script de configuration `inc/cfg_vars.*`**, l'option `install_list` est égale à `1` :

- **le script `bz.ksh*` doit être exécuté avec des droits administrateurs** - sinon il s’arrêtera - :
  - il copiera la liste pour unbound dans `/var/unbound/etc`,
  - et le fichier `hosts` dans '/etc' - il est nécessaire pour ce choix de paramétrer l'option `use_hosts` dans votre script de configuration.
  - et pour unwind, ce sera dans `/var/unwind.block`.

=> Le fichier `lists/personals` existe pour enregistrer vos propres choix de restrictions de domaines - un par ligne. /!\  **ATTENTION : ne fonctionne pas actuellement** /!\

=> Options de configuration : *cf: le fichier ''/dir/BlockZones/inc/cfg_vars.ksh''.*

- la variable `USE_LZ_REDIRECT` : pour 'unbound', sert à gérer si vous voulez l'ajout de la mention `local-zone "adr_ip" redirect`.
- la variable `install_list` : 
  - pour 'bind', 'hosts', 'unbound', copie la liste générée vers le répertoire approprié lié au service correspondant, si le service est utilisé. - **(25/08/2017)**
  - pour 'unwind' vers le répertoire approprié - **(29/05/2019)**
- la variable `use_hosts` pour permettre l'installation de la liste 'hosts' vers '/etc' - **(12/09/2018)**

----------

### Option `bogons`

=> L'option `bogons` peut récupérer les listes bogons IPv4 et/ou IPv6 mises-à-disposition par la Team Cymru. Il les traite pour que ce soit fonctionnel avec PF.

Une fois le script exécuté, retrouvez la liste traitée dans le répertoire `lists/`, avec son fichier de sommes de contrôle sha512.
- `bogons_ipv4` pour IPv4
- `bogons_ipv6` pour IPv6

**ATTENTION : Des problèmes avec la liste `fullbogons-ipv6.txt` de l'équipe Team Cymru, en entrée, sont remarqués** - *ce qui n'est pas le cas, en sortie...* <br/>
Elle peut induire des dysfonctionnements avec la couche ICMPv6. Si c'est votre cas, désactivez-la ! <br/>
/!\ **Désactivez-la, avant de chercher à résoudre vos problèmes de trafic IPv6** /!\

=> **Si dans le script de configuration `inc/cfg_vars.*`**, l'option `install_list` est égale à `1` :

- **le script `bz.ksh*` doit être exécuté avec des droits administrateurs** - sinon il s’arrêtera - :
  - afin de pouvoir copier les listes générées dans le répertoire `/etc/BlockZones`.
  - et de redémarrer la lecture des tables PF. Dans ce cas, **veillez IMPÉRATIVEMENT à créer/configurer des tables pour PF**, telles que les règles ci-dessous, **et à remplir les noms exacts dans le tableau `pf_tables` de votre script de configuration**. Une erreur dans un des noms des tables PF générera une erreur d'installation.

=> règles PF IPv4 :
```
table <t_bogons> persist file "/dir/bogons_ipv4"

block drop in quick on egress from { <t_bogons> } to any
block drop out quick on egress from any to { <t_bogons> }
```

=> règles PF IPv6 :
```
table <t_bogons6> persist file "/dir/bogons_ipv6"

block drop in quick on egress inet6 from { <t_bogons6> } to any
block drop out quick on egress inet6 from any to { <t_bogons6> }
```

/!\  **Attention** : Il peut être nécessaire d'agrandir le nombre des entrées de tables /!\

#### **Version Bash (Linux)**

À vous de gérer... pour le faire fonctionner avec iptables !

Tel que - *ce qui suit est un exemple* - :
```shell
while read -r line; do
    /sbin/iptables -I INPUT -s "${line}" -j DROP
    /sbin/iptables -I OUTPUT -d "${line}" -j DROP
done < /dir/BlockZones/lists/bogons_ipv4
```

*Note : Faites de même pour la liste ''bogons'' IPv6 :*
```shell
while read -r line; do
    /sbin/ip6tables -I INPUT -s "${line}" -j DROP
    /sbin/ip6tables -I OUTPUT -d "${line}" -j DROP
done < /dir/BlockZones/lists/bogons_ipv6
```

#### Autres informations :

/!\ Pensez à créer un fichier cron mensuel pour mettre-à-jour les informations, puis recharger PF. /!\

----------

### Option 'menu'

Cette option restitue un menu vous permettant de choisir la génération des listes...

----------

## Options de configuration

Certaines options sont modifiables depuis le fichier `/dir/BlockZones/inc/cfg_vars.ksh`.

**Astuce : Créez votre fichier `inc/cfg_vars.local` et recopiez les informations du fichier `inc/cfg_vars.ksh`. <br/> Ainsi, vos propres modifications ne seront pas annulées lors de mise-à-jour !**

/!\ *Ne modifiez que celles ci-dessous ; ATTENTION : les autres risques de créer des dysfonctionnements* /!\

Il est possible de modifier certains paramètres - *`0` pour désactiver* ou *`1` pour activer* - :

- utiliser les messages colorés
    - modifier la variable `use_color` : *`1` par défaut*
- utiliser l'interface Dialog
    - modifier la variable `dialog` : *`0` par défaut*

- utiliser les outils de téléchargement - s'ils sont à `0`, ce sera toujours `ftp` qui sera utilisé
    - modifier la variable `use_curl`: *`0` par défaut* - pour utiliser curl, s'il est installé
    - modifier la variable `use_wget`: *`0` par défaut* - pour utiliser wget, s'il est installé

- utiliser l'outil signify
    - modifier la variable `use_sign` : *`1` par défaut*
- créer un seul fichier de signature, et de sommes de contrôle sha512
    - modifier la variable `one_checksum_file` : *`1` par défaut*

- générer un journal debug
    - modifier la variable `debug` : *`0` par défaut*
    - modifier la variable `use_timestamp` : *`1` par défaut* ; **modifie le nom du fichier journal**

- utiliser dans une tâche cron -  **simplifie les messages, et désactive le mode couleur.**
    - modifier la variable `cron` : *`0` par défaut*
- utiliser le mode verbeux - **ne pas l'activer avec l'interface Dialog !**
    - modifier la variable `verbose` : *`0` par défaut*


La configuration par défaut du fichier `src/domains` suffit pour être gérée correctement par des services comme 'unbound'. <br/>
Si vous cherchez à gérer l'ensemble des urls, vous aurez le droit à des messages de dépassements de mémoire - ce qui signifie qu'il ne peut gérer l'ensemble de la liste que vous aurez créée !
<br/>
Ce "problème" ne se pose pas avec la gestion du fichiers `hosts`.


## Vérification des signatures

Deux fichiers de signature et de sommes de contrôles sha512 sont créées et déposées sur le dépôt, à-propos des différents codes. **(Juillet 2017)**

- le fichier ''BlockZones.pub'' est la clé publique de signature, lié au projet "BlockZones"
- le fichier ''BlockZones.sha512'' est le fichier de sommes de contrôles [sha512][3], pour tous les fichiers fournis par le projet
- le fichier ''BlockZones.sha512.sig'' est le fichier de signature, relatif au fichier précédent.

Pour vous assurer de la bonne signature, utilisez l'outil [signify(1)][2], de telle manière, à l'intérieur du répertoire parent du projet :

```$ signify -Cp BlockZones.pub -x BlockZones.sha512.sig```

Bien-sûr, l'outil `signify` n'est disponible par défaut que sous OpenBSD.
Sous Linux, les scripts ne génèrent que des fichiers de sommes de contrôle sha512 !

## Listes gérées

=> Les listes ''badips'' :
- la liste Autoshun - autoshun.org - **nécessaire de s'enregistrer avant l'usage** - *désactivée par défaut*
- les listes Abuse - abuse.ch - *(FeodoTracker, RansomwareTracker, ZeusTracker, SSLBL)*
- la liste Binary Defense - binarydefense.com - **pour un usage non commercial** - *(banlist)*
- la liste Blocklist - lists.blocklist.de
- la liste DShield - dshield.org
- les listes Emerging Threats - emergingthreats.net
- les listes FireHol - iplists.firehol.org
- la liste malwaredomainlist - malwaredomainlist.com
- la liste MyIP - myip.ms
- la liste OpenBL - openbl.org - *désactivée par défaut*
- la liste OSInt de BambenekConsulting - osint.bambenekconsulting.com
- les listes SpamHaus - spamhaus.org - *(drop, dropv6, edrop)*
- la liste shodan - isc.sans.edu
- et certainement bien d'autres...

=> Les listes ''bogons'' :

- Les listes de la Team Cymru - team-cymru.org
- deux listes personnelles : 'Martian Lists officielles', etc...
- et certainement bien d'autres...

=> Les listes ''domains'' :

- les listes Abuse - abuse.ch - *(FeodoTracker, RansomwareTracker, ZeusTracker)*
- la liste AdAway - adaway.org
- la liste Dan Pollock - someonewhocares.org - *désactivée par défaut*
- les listes DNS-BH - Black Hole domains - malwaredomains.com
- les listes HpHosts - hosts-file.net : **attention l'usage automatique est strictement interdit !**
- la liste malwaredomainlist - malwaredomainlist.com
- la liste PGL yoyo - pgl.yoyo.org - *désactivée par défaut*
- la liste Steven Black - stevenblack.com
- la liste winhelp2002 MVPS  - winhelp2002.mvps.org - **pour un usage non commercial**

----------

## Mémo Cron

/!\ Pensez à créer une tâche régulière hebdomadaire pour purger les "vieux" fichiers.

=> Pour les fichiers `/etc/hosts.bckp*` :

```find /etc/ -type f -name "hosts.bckp" -mtime +7 -delete```

=> Pour ''unbound'':

```find /var/unbound/etc/ -type f -name "local-zone.bckp*" -mtime +30 -delete```


## Mémo PF

Juste quelques infos pour mémos, à-propos de Packet-Filter, utiles dans ce contexte !

/!\ Pensez à créer une tâche régulière pour vider les tables PF, au besoin /!\

```# pfctl -t table_name -T expire nb_seconds```

=> recharger une des tables - on recharge PF :

```# pfctl -t table_name -T replace -f /etc/BlockZones/file_table_name```

=> augmenter le nombre d'entrées de table - éditer `/etc/pf.conf` - c'est juste un exemple - :

```set limit table-entries 300000```

=> Si vous utilisez les listes ''bogons'', et ''badips'', pensez à optimisez vos règles PF, tel que, par exemple :

```
block drop in quick on egress from { <t_badips>, <t_bogons> } to any
block drop out quick on egress from any to { <t_badips>, <t_bogons> }
```

Et, idem pour les règles IPv6, bien sûr !

----------

[1]: https://stephane-huc.net/share/BlockZones/lists/
[2]: http://man.openbsd.org/signify
[3]: http://man.openbsd.org/sha512

*[IP]: Internet Protocol
*[PF]: Packet Filter

