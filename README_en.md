# BlockZones

Project to blacklist bad domains names, and bad addresses IP, *knew for bad activities*:
- ADS servers
- malwares, trackers, and others badlies
- bogons
- and others badips

**Thoses lists are updated all days, with checksums sha512 files, and sign files, at:**<br/>
[https://stephane-huc.net/share/BlockZones/lists/][1]

/!\ **WARNING: deeply recoding; only on script for all actions: bz.ksh{93}** /!\

----------

## Usage
```shell
Usage: ./bz.ksh options
    Options:
    -b: to generate bogons lists!
    -h: to display this help.
    -i: to generate badips lists!
    -l: to generate blacklists lists; arguments are 'unbound', 'hosts', 'host0', 'bind8', 'bind9', 'unwind'...
    -m: to use this script with a menu...
```
This script exists only on pdksh version, for OpenBSD, and ksh93 for \*BSD.

----------

### Option `badips`

=> Script `badips` acts in several times:

- downloads wroten lists, into file `src/badips`, with a latence.
- threats them to create two single lists, one `lists/badips\_ipv4` et one `lists/badips\_ipv6`, with sha512 checksums file

Copy both files where you want before manage them by PF!

=> **If into file config `inc/cfg_vars.*`**, the option `install_list` egual to `1` :

- **the script `bz.ksh*` need to run with admins rights** - else will stop - :
  - to copy generated lists into folder `/etc/BlockZones`.
  - and, reread tables by PF. In this cas, it's NECESSARY to create|configure tables for PF - see, for example, rules below - and write theirs names EXACTLY into the array `pf_tables` in the file config. **One error on the name of table PF will result error of initialization.**

=> IPv4 rules PF:
```
table <t_badips> persist file "/dir/badips_ipv4"

block drop in quick on egress from { <t_badips> } to any
block drop out quick on egress from any to { <t_badips> }
```
=> IPV6 rules PF:
```
table <t_badips6> persist file "/dir/badips_ipv6"

block drop in quick on egress inet6 from { <t_badips6> } to any
block drop out quick on egress inet6 from any to { <t_badips6> }
```

**ATTENTION**: It seems necessary to grow the number entries of PF's tables...

----------

### Option `blacklist`

=> Script `blacklist` acts in several times:

- downloads authorized lists, wroten into file `src/domains`, with a latence.
- creates one file:
  - `lists/local-zone` to threat with unbound,
  - `lists/bind.zone` to threat by bind (v8, v9),
  - `lists/hosts` for local threat with '/etc/hosts'...
  - `lists/baddomains` to threat with unwind - *since OpenBSD 6.5*
- and create checksums files sha512!

Arguments are:

- `unbound`,
- `bind8` or `bind9`
- `hosts` for the file `/etc/hosts`, or `host0` egual for the `/etc/hosts` but with address `0.0.0.0` and not `127.0.0.1`
- `unwind`

**ATTENTION**: If you active all wroten URL into `src/domains`, it possible the service that has to process the final single list can not do so because of a lack of memory resources. <br/> Egual, this will grow threatment time to create single list by your computer.

=> **If into file config `inc/cfg_vars.*`**, the option `install_list` egual to `1` :

- **the script `bz.ksh*` need to run with admins rights** - else will stop - :
  - will copy the list for unbound into `/var/unbound/etc`,
  - and, the `hosts` into `/etc` - it's NECESSARY to use this choice to set the option `use_hosts` into your file config.
  - and, the `unwind.block` into `/var/`.

=> The `lists/personals` exists to save yours personals choices to restrict some domains: one by line. <br/> /!\  **ATTENTION : actually, not run correctly!** /!\

=> Config options : *see file ''/dir/BlockZones/inc/cfg_vars.ksh''.*

- variable `USE_LZ_REDIRECT`: only for 'unbound', if you want `local-zone "adr_ip" redirect` information into the list.
- variable `install_list`: 
  - for 'bind', 'hosts', 'unbound', copy the generated list to directory of the related service. - **(2017/08/25)**
  - for 'unwind', to the good directory - **(2019/05/29)**
- variable `use_hosts`: to auth the generated list `hosts` to `/etc` - **(2018/09/12)**

----------

### Option `bogons`

=> The `bogons` script can get both bogons (IPv4, IPv6) lists, available by Team Cymry. It threats to manage them by PF.

Once this script created lists, see `lists/`:

- `bogons_ipv4` for IPv4
- `bogons_ipv6` for ÏPv6
- and checksums files

**ATTENTION: Some problems, with 'fullbogons-ipv6' list (by Team Cymru), in input, noticied.** - *this seems not case, in output...* <br/> It can trouble ICMPv6 layer. If you experiment, disable! 

/!\ **Disable, before search to resolve yours troubles trafic IPv6** /!\

=> **If into file config `inc/cfg_vars.*`**, the option `install_list` egual to `1` :

- **the script `bz.ksh*` need to run with admins rights** - else will stop - :
  - to copy generated lists into folder `/etc/BlockZones`.
  - and, re-read tables by PF. In this cas, it's NECESSARY to create/configure tables for PF - see, for example, rules below - and write theirs names EXACTLY into the array `pf_tables` in the file config. <br/> **One error on the name of table PF will result error of initialization.**

=> IPv4 rules PF:
```
table <t_bogons> persist file "/dir/bogons_ipv4"

block drop in quick on egress from { <t_bogons> } to any
block drop out quick on egress from any to { <t_bogons> }
```

=> IPv6 rules PF:
```
table <t_bogons6> persist file "/dir/bogons_ipv6"

block drop in quick on egress inet6 from { <t_bogons6> } to any
block drop out quick on egress inet6 from any to { <t_bogons6> }
```

**ATTENTION**: it seems necessary to grow the entries number of tables!

#### **Bash (Linux) Version**

It's up to you... to manage with iptables!

*this is an example*:
```
while read -r line; do
    /sbin/iptables -I INPUT -s "${line}" -j DROP
    /sbin/iptables -I OUTPUT -d "${line}" -j DROP
done < /dir/BlockZones/lists/bogons_ipv4
```

*Note: egual for bogons IPv6 list:*
```
while read -r line; do
    /sbin/ip6tables -I INPUT -s "${line}" -j DROP
    /sbin/ip6tables -I OUTPUT -d "${line}" -j DROP
done < /dir/BlockZones/lists/bogons_ipv6
```

#### **Others informations:**

/!\ Think to create a monthly task cron to update informations, and after reload PF. /!\

----------

### Option 'menu'

This option display a menu to choose the action to generate lists...

----------

## Config options

Somes options are setting into ''/dir/BlockZones/inc/cfg_vars.ksh'' file.

**Tips: Create your owner file ''inc/cfg_vars.local'', and copy into all informations of ''inc/cfg_vars.ksh'' file. <br/> Also, yours modifications are not canceled when during update!**

/!\ *Set only them below; ATTENTION: others risk to create troubles.* /!\

It is possible to set few paramaters - *`0` to disable; `1` to enable* -:

- to use colored messages
    - set variable `use_color`; *default: `1`*
- to use Dialog interface
    - set variable `dialog`; *default: `0`*

- to use downloader tools - if they configured to `0`, always only ftp will use!
    - set variable `use_curl`; *default: `0`*; to use curl, if installed
    - set variable `use_wget`; *default: `0`*; to use wget, if installed

- to use signify tool
    - set variable `use_sign`; *default: `1`*
- to create only one sign file, and checksums sha512 file
    - set variable `one_checksum_file`; *default: `1`*

- to create a log debug
    - set variable 'debug'; *default: 0*
    - set variable 'use_timestamp': *default: 1* ; **modify filename log**

- to use into cron task - **Kiss messages, and disable colored messages.**
    - set variable `cron`; *default: `0`*
- to use verbose mode - **DO NOT enable with dialog interface.**
    - set variable `verbose`; *default: `0`*

By default, the config of the `src/domains` is enough to manage correctly service as 'unbound'.
It is possible you have lack of memories ressource, if you set all urls. If that is, reduce number urls.
<br/> This "problem" not exists with 'hosts' file.

The default config manage quasi 65000 bad urls. The fully: ~ 500K!

## Check sign

Two checksums sha512 and sign files are created, about project code. **(July 2017)**

- ''BlockZones.pub'': public key sign file, related to "BlockZones" Project,
- ''BlockZones.sha512'': checksums [sha512][3] file, to verify all project files,
- ''BlockZones.sha512.sign'': signify file.

To check good sign, use [signify(1)][2], at root project:

```$ signify -Cp BlockZones.pub -x BlockZones.sha512.sig```

Of course, the 'signify' tool is available, by default, only on OpenBSD.
Under Linux, scripts create only checksums sha512 files!

## Managed lists

=> Lists ''badips'':
- Autoshun - autoshun.org - **need to register** - *by default, disable*
- Abuse - abuse.ch - *(FeodoTracker, RansomwareTracker, ZeusTracker, SSLBL)*
- Binary Defense - binarydefense.com - **only, for personal use** - *(banlist)*
- Blocklist - lists.blocklist.de
- DShield - dshield.org
- Emerging Threats - emergingthreats.net
- FireHol - iplists.firehol.org
- malwaredomainlist - malwaredomainlist.com
- MyIP - myip.ms
- OpenBL - openbl.org - *by default, disable*
- OSInt de BambenekConsulting - osint.bambenekconsulting.com
- SpamHaus - spamhaus.org - *(drop, dropv6, edrop)*
- shodan - isc.sans.edu
- and, certainly, many others...

=> Lists ''bogons'':

- Team Cymru - team-cymru.org
- Two personals : 'Officials Martian Lists', etc.
- and, certainly, many others...

=> Lists ''domains'':

- Abuse - abuse.ch - *(FeodoTracker, RansomwareTracker, ZeusTracker)*
- AdAway - adaway.org
- Dan Pollock - someonewhocares.org - *(by default: disabled; included into Steven Black Lists)*
- DNS-BH - Black Hole domains - malwaredomains.com
- HpHosts - hosts-file.net : **attention: automatic use is strictly forbidden!**
- malwaredomainlist - malwaredomainlist.com
- PGL yoyo - pgl.yoyo.org - *(by default: disabled; included into Steven Black Lists)*
- Steven Black - stevenblack.com
- winhelp2002 MVPS  - winhelp2002.mvps.org - **only, for personal use**

----------

## Cron Notes

/!\ Think to create a weekly task cron to purges oldiers files

=> for `/etc/hosts.bckp*`:

```find /etc/ -type f -name "hosts.bckp" -mtime +7 -delete```

=> for ''unbound'':

```find /var/unbound/etc/ -type f -name "local-zone.bckp*" -mtime +30 -delete```

## PF Notes

Some notes, about Packet-Filter!

/!\ Think to create a regular task cron to flush tables PF. /!\

```# pfctl -t table_name -T expire nb_seconds```

=> to reload one table, reload PF :

```# pfctl -t table_name -T replace /etc/BlockZones/file_table_name```

=> to grow entries number table, by edit `/etc/pf.conf` - just, as example -:

```set limit table-entries 300000```

=> If you use ''bogons'', and ''badips'' lists, think to optimize yours rules PF - e.g:

```
block drop in quick on egress from { <t_badips>, <t_bogons> } to any
block drop out quick on egress from any to { <t_badips>, <t_bogons> }
```

And, same works for IPv6 rules!

----------

[1]: https://stephane-huc.net/share/BlockZones/lists/
[2]: http://man.openbsd.org/signify
[3]: http://man.openbsd.org/sha512

*[IP]: Internet Protocol
*[PF]: Packet Filter

